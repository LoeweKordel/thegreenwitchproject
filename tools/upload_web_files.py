import os
import sys
import io
import time
import re
import shutil
from typing import List

from ftplib import FTP
import htmlmin
import jsmin

from default_ip import getIP


def MinimizeCSS(css: str) -> str:
    # remove comments - this will break a lot of hacks :-P
    css = re.sub(r'\s*/\*\s*\*/', "$$HACK1$$", css) # preserve IE<6 comment hack
    css = re.sub(r'/\*[\s\S]*?\*/', "", css)
    css = css.replace("$$HACK1$$", '/**/') # preserve IE<6 comment hack

    # url() doesn't need quotes
    css = re.sub(r'url\((["\'])([^)]*)\1\)', r'url(\2)', css)

    # spaces may be safely collapsed as generated content will collapse them anyway
    css = re.sub(r'\s+', ' ', css)

    # shorten collapsable colors: #aabbcc to #abc
    css = re.sub(r'#([0-9a-f])\1([0-9a-f])\2([0-9a-f])\3(\s|;)', r'#\1\2\3\4', css)

    # fragment values can loose zeros
    css = re.sub(r':\s*0(\.\d+([cm]m|e[mx]|in|p[ctx]))\s*;', r':\1;', css)

    output: List[str] = []

    for rule in re.findall(r'([^{]+){([^}]*)}', css):

        # we don't need spaces around operators
        selectors = [re.sub(r'(?<=[\[\(>+=])\s+|\s+(?=[=~^$*|>+\]\)])', r'', selector.strip()) for selector in rule[0].split(',')]

        # order is important, but we still want to discard repetitions
        properties = {}
        porder = []
        for prop in re.findall('(.*?):(.*?)(;|$)', rule[1]):
            key = prop[0].strip().lower()
            if key not in porder:
                porder.append(key)
            properties[ key ] = prop[1].strip()

        # output rule if it contains any declarations
        if properties:
            output.append(f"{','.join(selectors)}{{{''.join(['%s:%s;' % (key, properties[key]) for key in porder])[:-1]}}}")
    return "\n".join(output)


def MinimizeFiles():
    print("Minimize files...")
    html = open("html/index.html").read()
    html = htmlmin.minify(html, remove_comments=True, remove_all_empty_space=True,
                          remove_optional_attribute_quotes=False)
    if not os.path.exists("html.min"):
        os.mkdir("html.min")
    with open("html.min/index.html", "w") as F:
        F.write(html)
    with open("html.min/index_html.h", "w") as F:
        F.write('const char INDEX_HTML[] PROGMEM = R"rawliteral(\n')
        F.write(html)
        F.write('\n)rawliteral";\n')
    css = open("html/main.css").read()
    css = MinimizeCSS(css)
    with open("html.min/main.css", "w") as F:
        F.write(css)
    with open("html.min/main_css.h", "w") as F:
        F.write('const char MAIN_CSS[] PROGMEM = R"rawliteral(\n')
        F.write(css)
        F.write('\n)rawliteral";\n')
    shutil.copyfile("html/favicon.png", "html.min/favicon.png")
    for jsFile in ("main.js", "multisensor.js"):
        js = open(f"html/{jsFile}").read()
        js = jsmin.jsmin(js, quote_chars="'\"`")
        with open(f"html.min/{jsFile}", "w") as F:
            F.write(js)
        with open(f"html.min/{jsFile.replace('.', '_')}.h", "w") as F:
            F.write(f'const char {jsFile.upper().replace(".", "_")}[] PROGMEM = R"rawliteral(\n')
            F.write(js)
            F.write('\n)rawliteral";\n')


def UploadFile(dirName: str, fileName: str, data: str):
    for _ in range(5):
        print(f"Upload {dirName}/{fileName} (size={len(data)})...")
        ftp.storbinary(f"STOR web/{fileName}", io.BytesIO(data))
        buffer = io.BytesIO()
        ftp.retrbinary(f"RETR web/{fileName}", buffer.write)
        if buffer.getvalue() == data:
            return True
        print(f"Failed to upload {fileName} (local={len(buffer.getvalue())} remove={len(data)})! Try again...")
        time.sleep(1)
    print("Failed! Giving up.")
    return False


if __name__ == "__main__":
    MinimizeFiles()
    files = ("index.html", "main.css", "main.js", "multisensor.js", "favicon.png")
    dirName = "html.min"
    if "--files" in sys.argv:
        idx = sys.argv.index("--files")
        del sys.argv[idx]
        files = sys.argv.pop(idx).split(",")
    ip = getIP()
    print(f"IP: {ip}")
    htmlDirName = dirName if os.path.exists(dirName) else f"../{dirName}"
    with FTP(ip, timeout=10) as ftp:
        print("Login...")
        ftp.login("MS01", "MS01")
        print("mkdir web")
        ftp.mkd("web")
        for fileName in files:
            with open(f"{htmlDirName}/{fileName}", 'rb') as F:
                data = F.read()
            if not UploadFile(htmlDirName, fileName, data):
                sys.exit(1)
    print("Finished.")
