import asyncio
import json
from typing import List

import websockets

from default_ip import getIP


async def GetFileList(ip: str) -> List[str]:
    print("Get SD-Card contents...")
    async with websockets.connect(f"ws://{ip}/ws") as ws:
        await ws.recv()
        offset = 0
        allFileNames = []
        while True:
            await ws.send(f"list_sdcard_files {offset}")
            result = await ws.recv()
            if not result:
                break
            data = json.loads(result)
            if data.get("filename_nr") == -1:
                break
            fileNames = data.get("filenames")
            if fileNames:
                fileNames = [fileName[1:] for fileName in fileNames.splitlines()]
                offset += len(fileNames)
                allFileNames.extend(fileNames)
                print(f"{offset} files")
    return allFileNames


if __name__ == "__main__":
    ip = getIP()
    print(f"IP: {ip}")
    remoteFileNames = asyncio.run(GetFileList(ip))
    print("\n".join(remoteFileNames))
