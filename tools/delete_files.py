import os
import sys
from datetime import date
from ftplib import FTP

from default_ip import getIP

if __name__ == "__main__":
    ip = getIP()
    print(f"IP: {ip}")
    with FTP(ip) as ftp:
        print("Login...")
        ftp.login("MS01", "MS01")
        print("Get SD-Card contents...")
        fileNames = [fileName for fileName in ftp.nlst("/") if fileName.endswith(".bin")]
        if not fileNames:
            print("No files to delete.")
            sys.exit(0)
        for nr, fileName in enumerate(fileNames):
            print(f"Delete {fileName}")
            ftp.delete(fileName)
    print("Finished.")
