import serial

if __name__ == "__main__":
    ser = serial.Serial('/dev/ttyUSB0', 230400, timeout=3)
    print("SD-Card contents:")
    ser.write(b'l')
    fileNames = []
    while True:
        line = ser.readline()
        if not line:
            break
        if line.startswith(b"::OK"):
            break
        if line.startswith(b"::"):
            fileName = line[2:].strip().decode("utf-8")
            print(fileName)
            fileNames.append(fileName)
    print(f"{len(fileNames)} files")
    ser.close()
