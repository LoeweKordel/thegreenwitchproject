import os
import sys
from ftplib import FTP

from default_ip import getIP

if __name__ == "__main__":
    ip = getIP()
    dirName = sys.argv[1]
    print(f"IP: {ip}")
    if not os.path.exists(dirName):
        print("Enter existing directory!")
        sys.exit(1)
    fileNames = [fileName for fileName in os.listdir(dirName) if fileName.endswith(".bin")]
    if not fileNames:
        print("Directory does not contain any bin-file!")
        sys.exit(2)
    with FTP(ip, timeout=10) as ftp:
        print("Login...")
        ftp.login("MS01", "MS01")
        print("Get filelist on SD-Card.")
        remoteFileNames = [fileName for fileName in ftp.nlst("/") if fileName.endswith(".bin")]
        fileNames = sorted(set(fileNames).difference(remoteFileNames))
        print(f"Upload {len(fileNames)} data files to {ip}...")
        for nr, fileName in enumerate(fileNames):
            print(f"Upload {nr + 1}/{len(fileNames)} {fileName}...")
            ftp.storbinary(f"STOR {fileName}", open(f"{dirName}/{fileName}", 'rb'))
    print("Finished.")
