import json
import time

from websocket import create_connection

from default_ip import getIP

if __name__ == "__main__":
    ip = getIP()
    print(f"IP: {ip}")
    ws = create_connection(f"ws://{ip}/ws")
    print("Monitor Memory Status...")
    try:
        while True:
            ws.send("get_memory_status")
            while True:
                result = json.loads(ws.recv())
                if "free_heap_size" in result:
                    print(f"free_heap_size={result['free_heap_size']}  int_free_heap_size={result['int_free_heap_size']}  min_free_heap_size={result['min_free_heap_size']}")
                    time.sleep(1)
                    break
    except KeyboardInterrupt:
        pass
    ws.close()
