from ftplib import FTP

from default_ip import getIP

if __name__ == "__main__":
    ip = getIP()
    print(f"IP: {ip}")
    with FTP(ip, timeout=10) as ftp:
        print("Login...")
        ftp.login("MS01", "MS01")
        print("SD-Card contents:")
        fileNames = ftp.nlst("/")
        print("  " + "\n  ".join(fileNames))
        print(f"{len(fileNames)} files.")
