# -*- coding: utf-8 -*-
# Author: Martin Bammer

import json
from typing import List

from PySide6.QtCore import Slot, QFile, QIODevice, QObject, Signal
from PySide6.QtWebEngineCore import QWebEngineSettings, QWebEnginePage
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWebChannel import QWebChannel


class Backend(QObject):
    connectedChanged = Signal(bool)
    currentFilenameChanged = Signal(str)
    filenamesChanged = Signal(int, object)
    deviceChanged = Signal(object)
    offsetsChanged = Signal(object)
    historyChanged = Signal(object)
    haarpChanged = Signal(object)
    sds011InfoChanged = Signal(object)
    dataChanged = Signal(dict)

    def __init__(self, parent=None):
        super().__init__(parent)
        self._bConnected: bool = False
        self._currentFilename: str = ""
        self._filenames: List[str] = []
        self._device: str = ""
        self._haarp: str = ""
        self._offsets: str = ""
        self._history: str = ""
        self._sds011Info: str = ""
        self._data: dict = {}

    @property
    def isConncted(self):
        return self._bConnected

    @Slot(str)
    def setConnected(self, bConnected: str):
        self._bConnected = bool(bConnected)
        self.connectedChanged.emit(bool(bConnected))

    @property
    def currentFilename(self):
        return self._currentFilename

    @Slot(str)
    def setCurrentFilename(self, currentFilename: str):
        if currentFilename != self._currentFilename:
            print("setCurrentFilename", currentFilename)
            self._currentFilename = currentFilename
            self.currentFilenameChanged.emit(self._currentFilename)

    @property
    def filenames(self):
        return self._filenames

    @Slot(str)
    def setDevice(self, data: str):
        # print("setDevice", data)
        data = json.loads(data)
        self._device = data
        self.deviceChanged.emit(self._device)

    @Slot(str)
    def setOffsets(self, data: str):
        # print("setOffsets", data)
        data = json.loads(data)
        self._offsets = data
        self.offsetsChanged.emit(self._offsets)

    @Slot(str)
    def setHistory(self, data: str):
        # print("setHistory", data)
        data = json.loads(data)
        self._history = data
        self.historyChanged.emit(self._history)

    @Slot(str)
    def setHAARP(self, data: str):
        # print("setHAARP", data)
        data = json.loads(data)
        self._haarp = data["haarp"]
        self.haarpChanged.emit(self._haarp)

    @Slot(str)
    def setSDS011Info(self, data: str):
        # print("setSDS011Info", data)
        data = json.loads(data)
        self._sds011Info = data["sds011_info"]
        self.sds011InfoChanged.emit(self._sds011Info)

    @Slot(str)
    def setFilenames(self, data: str):
        # print("setFilenames", data)
        data = json.loads(data)
        if "filenames" not in data:
            return
        nr = data["filename_nr"]
        if nr > 0 and nr & 0x80000000:
            nr = (nr ^ 0xffffffff) - 1
        filenames = data["filenames"].split("\n")
        if nr >= 0:
            if nr < len(self._filenames):
                self._filenames = filenames
            else:
                self._filenames.extend(filenames)
        self.filenamesChanged.emit(nr, filenames)

    @property
    def data(self):
        return self._data

    @Slot(str)
    def setData(self, data: str):
        if not data:
            return
        self._data = json.loads(data)
        self.dataChanged.emit(self._data)


class MultiSensorPage(QWebEnginePage):

    def __init__(self, parent=None):
        super().__init__(parent)
        self._backend = Backend(self)
        self.loadFinished.connect(self.onLoadFinished)

    def javaScriptConsoleMessage(self, level, message, lineNumber, sourceID):
        print(level, lineNumber, sourceID, message)

    @property
    def backend(self) -> Backend:
        return self._backend

    @Slot(bool)
    def onLoadFinished(self, success: bool):
        if success:
            self.loadQWebChannel()
            self.loadObject()

    def loadQWebChannel(self):
        print("loadQWebChannel")
        file = QFile(":/qtwebchannel/qwebchannel.js")
        if file.open(QIODevice.ReadOnly):
            content = file.readAll()
            file.close()
            self.runJavaScript(content.data().decode())
        if self.webChannel() is None:
            self.setWebChannel(QWebChannel(self))

    def loadObject(self):
        print("loadObject")
        if self.webChannel() is not None:
            self.webChannel().registerObject("backend", self.backend)
            script = r"""new QWebChannel(qt.webChannelTransport, function (channel) {
                            var backend = channel.objects.backend;
                            var page = document.getElementById("connection");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    console.log("###connection changed");
                                    backend.setConnected(event.detail.data);
                                });
                            }
                            var page = document.getElementById("filenames");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    console.log("###filenames changed");
                                    backend.setFilenames(event.detail.data);
                                });
                            }
                            var page = document.getElementById("device_page");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    console.log("###device changed");
                                    backend.setDevice(event.detail.data);
                                });
                            }
                            var page = document.getElementById("offsets");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    console.log("###offsets changed");
                                    backend.setOffsets(event.detail.data);
                                });
                            }
                            var page = document.getElementById("history_page");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    // console.log("###history changed");
                                    backend.setHistory(event.detail.data);
                                });
                            }
                            var page = document.getElementById("haarp_page");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    // console.log("###haarp changed");
                                    backend.setHAARP(event.detail.data);
                                });
                            }
                            var page = document.getElementById("sds011_info");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    console.log("###sds011_info changed");
                                    backend.setSDS011Info(event.detail.data);
                                });
                            }
                            var page = document.getElementById("sensordata");
                            if (page != null) {
                                page.addEventListener("change", function(event) {
                                    // console.log("###sensordata changed");
                                    backend.setData(event.detail.data);
                                });
                            }
                         });"""
            self.runJavaScript(script)

    def toggleBacklight(self):
        script = r"""var button = document.getElementById('toggle_backlight');
                     if (button != null) {
                         button.dispatchEvent(new Event('click'));
                     }"""
        self.runJavaScript(script)

    def toggleRecording(self):
        script = r"""var button = document.getElementById('toggle_recording');
                     if (button != null) {
                         button.dispatchEvent(new Event('click'));
                     }"""
        self.runJavaScript(script)

    def getCurrentFilename(self):
        # print("getCurrentFilename")
        script = r"""new QWebChannel(qt.webChannelTransport, function (channel) {
                        var backend = channel.objects.backend;
                        var filename = document.getElementById("filename");
                        if (filename != null) {
                            backend.setCurrentFilename(filename.innerText);
                        }
                        });"""
        self.runJavaScript(script)

    def getHistory(self):
        # print("getHistory")
        self.runJavaScript("getHistory();")

    def getSdCardContents(self):
        # print("getSdCardContents")
        self.runJavaScript("getSdCardContents(0);")


class MultiSensorView(QWebEngineView):
    pageLoaded = Signal(bool)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.AllowRunningInsecureContent, True)
        self.loadProgress.connect(print)
        self.loadFinished.connect(self.pageReady)
        self.setPage(MultiSensorPage(self))

    def pageReady(self, success: bool):
        print("pageReady", success)
        self.pageLoaded.emit(success)
        if success:
            self.show()
        else:
            print('pageReady: Page failed to load')

    @property
    def backend(self) -> Backend:
        return self.page().backend

    def toggleBacklight(self):
        self.page().toggleBacklight()

    def toggleRecording(self):
        self.page().toggleRecording()

    def getCurrentFilename(self):
        self.page().getCurrentFilename()

    def getHistory(self):
        self.page().getHistory()

    def getSdCardContents(self):
        self.page().getSdCardContents()
