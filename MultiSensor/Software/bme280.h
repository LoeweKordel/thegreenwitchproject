#ifndef _BME280_H
#define _BME280_H

#include <Wire.h>
#include <Bme280.h>

#include "float16.h"
#include "common.h"

// Interface is I2C

Bme280TwoWire bme280;

void resetBME280() {
  bme280.reset();
  delay(10);
  Bme280Settings settings = Bme280Settings::weatherMonitoring();
  settings.mode = Bme280Mode::Normal;
  bme280.setSettings(settings);
}

bool setupBME280(int16_t y) {
  Wire.begin();
  bme280.begin(Bme280TwoWireAddress::Primary);
  resetBME280();
  return true;
}

void updateBME280(bme280_t *p) {
  temp.bme280 = bme280.getTemperature();
  humidity.bme280 = bme280.getHumidity();
  p->temp = lround(temp.bme280 * 100.0); // °C
  p->humidity = lround(humidity.bme280 * 10.0); // %
  temp.bme280 +=  offsets.bme280_temp;
  humidity.bme280 += offsets.bme280_humidity;
  if (SerialOut > 1) {
    sprintf(buffer, "I:BME280: Temp=%.2f Hum=%.1f", temp.bme280, humidity.bme280);
    Serial.println(buffer);
  }
}

void updateBME280Pressure(bme280_t *p) {
  if (p->cnt >= PRESSURE_SIZE) {
    return;
  }
  pressure.bme280 = bme280.getPressure() / 100.0;
  p->pressure[p->cnt++] = f32tof16(pressure.bme280); // hPa
  pressure.bme280 += offsets.bme280_pressure;
}

#endif // _BME280_H
