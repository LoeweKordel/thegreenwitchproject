#ifndef _FTP_SERVER_H 
#define _FTP_SERVER_H

#include <WiFi.h>

#include "ESP32FtpServer.h"

FtpServer ftpSrv;

void setupFtpServer() {
  Serial.println("I:setupFtpServer");
  ftpSrv.begin("MS01","MS01"); // username, password for ftp.  set ports in ESP8266FtpServer.h  (default 21, 50009 for PASV)
}

bool isFtpClientConnected() {
  return ftpSrv.isClientConnected();
}

int8_t getFtpTransferStatus() {
  return ftpSrv.getTransferStatus();
}

void handleFtpServer() {
  ftpSrv.handleFTP();
}

void setFtpDebugMode(boolean debugMode) {
  ftpSrv.debugMode = debugMode;
}

#endif // _FTP_SERVER_H
