#ifndef _WEB_H
#define _WEB_H

#include <time.h>
#include <esp32-hal.h>
#include <esp_sntp.h>
#include <WiFiType.h>
#include <WiFi.h>
#include <AsyncTCP_SSL.h>
#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include <ArduinoJson.h>

#include "common.h"
#include "bitmap.h"
#include "clock.h"
#include "ftpserver.h"
#include "sd.h"
#include "imu9dof.h"
#include "mhz19.h"

// secret.h contains:
// const char* ssid     = "<NetworkID>";
// const char* password = "<PWD>";
#include "secret.h"

bool ResetSensors = false;
wl_status_t WiFiStatus = WL_DISCONNECTED;
uint8_t *wifi_icon = (uint8_t *)ico_wifi_very_good;
uint32_t ws_counter = 0;
uint32_t ws_hist_counter[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
DynamicJsonDocument doc(5120);

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

void sendHistory(String key);
void sendDataWs(bool force);

char* addU16Hex(char *buf, uint16_t value) {
  for (uint8_t i = 0; i < 4; i++) {
    char c = value & 0xf;

    if (c < 10) {
      *buf++ = '0' + c;
    } else {
      *buf++ = 'a' + c - 10;
    }
    value >>= 4;
  }
  return buf;
}

char* addI16Hex(char *buf, int16_t value) {
  return addU16Hex(buf, (uint16_t)value);
}

bool sendWs(const char *title) {
  if (esp_get_free_heap_size() < 16384) {
    for (uint8_t i = 0; i < 10; i++) {
      yield();
      delay(50);
      if (SerialOut > 1) {
        sprintf(buffer, "I:%d", esp_get_free_heap_size());
        Serial.println(buffer);
      }
      if (esp_get_free_heap_size() >= 16384) break;
      if (i == 9) return false;
    }
  }
  size_t len = serializeJson(doc, buffer);
  if (SerialOut > 1) {
    sprintf(buffer, "I:%s %d", title, len);
    Serial.println(buffer);
  }
  ws.textAll(buffer);
  return true;
}

void sendMemoryStatus() {
  doc.clear();
  doc["free_heap_size"] = esp_get_free_heap_size();
  doc["int_free_heap_size"] = esp_get_free_internal_heap_size();
  doc["min_free_heap_size"] = esp_get_minimum_free_heap_size();
  sendWs("sendMemoryStatus");
}

void sendOk(char *msg) {
  doc.clear();
  doc["result"] = msg;
  sendWs(msg);
}

void toggleRecording() {
  RecordingEnabled = !RecordingEnabled;
  doc.clear();
  doc["result"] = "toggle_recording";
  doc["recording"] = RecordingEnabled;
  sendWs("toggleRecording");
}

void toggleDebugMode() {
  DebugMode = !DebugMode;
  setFtpDebugMode(DebugMode);
  doc.clear();
  doc["result"] = "toggle_debug_mode";
  doc["debug"] = DebugMode;
  sendWs("toggleDebugMode");
}

void toggleSerialOut() {
  SerialOut = (SerialOut + 1) % 3;
  doc.clear();
  doc["result"] = "toggle_serial_out";
  doc["serial"] = SerialOut;
  sendWs("toggleSerialOut");
}

void toggleBacklight() {
  setBacklight(!BacklightEnabled);
  doc.clear();
  doc["result"] = "toggle_backlight";
  doc["bl"] = BacklightEnabled;
  sendWs("toggleBacklight");
}

void clearHistory() {
  data_cnt = 0;
  counter = 0;
  ws_counter = 0;
  for (int i = 0; i < 12; i++) {
    ws_hist_counter[i] = 0;
  }
}

void addXYZHistory(char *key, JsonObject &obj, xyz_t *array, uint8_t cnt) {
  JsonObject xyzJson = obj.createNestedObject(key);
  char *buf = buffer;

  for (int i = 0; i < cnt; i++) {
    buf = addU16Hex(buf, array[i].x);
  }
  *buf = 0;
  xyzJson["x"] = buffer;

  buf = buffer;
  for (int i = 0; i < cnt; i++) {
    buf = addU16Hex(buf, array[i].y);
  }
  *buf = 0;
  xyzJson["y"] = buffer;

  buf = buffer;
  for (int i = 0; i < cnt; i++) {
    buf = addU16Hex(buf, array[i].z);
  }
  *buf = 0;
  xyzJson["z"] = buffer;
}

void addUArray(char *key, JsonObject &obj, uint16_t *array, uint8_t cnt) {
  char *buf = buffer;

  for (int i = 0; i < cnt; i++) {
    buf = addU16Hex(buf, array[i]);
  }
  *buf = 0;
  obj[key] = buffer;
}

void addArray(char *key, JsonObject &obj, int16_t *array, uint8_t cnt) {
  char *buf = buffer;

  for (int i = 0; i < cnt; i++) {
    buf = addI16Hex(buf, array[i]);
  }
  *buf = 0;
  obj[key] = buffer;
}

void addBMP180Sensor(char *key, JsonObject &history, bmp180_t &bmp180) {
  JsonObject childJson = history.createNestedObject(key);

  addUArray((char *)"pressure", childJson, bmp180.pressure, bmp180.cnt);
}

void addBME280Sensor(char *key, JsonObject &history, bme280_t &bme280) {
  JsonObject childJson = history.createNestedObject(key);

  addUArray((char *)"pressure", childJson, bme280.pressure, bme280.cnt);
}

void addTLV493DSensor(char *key, JsonObject &history, tlv493d_t &tlv493d) {
  JsonObject childJson = history.createNestedObject(key);

  addXYZHistory((char *)"magnet", childJson, tlv493d.magnet, tlv493d.cnt);
}

void addIMU9DOFSensor(char *key, JsonObject &history, imu9dof_t &imu9dof) {
  JsonObject childJson = history.createNestedObject(key);

  addXYZHistory((char *)"accel", childJson, imu9dof.accel, imu9dof.icm20600_cnt);
  addXYZHistory((char *)"gyro", childJson, imu9dof.gyro, imu9dof.icm20600_cnt);
  addXYZHistory((char *)"magnet", childJson, imu9dof.magnet, imu9dof.ak09918_cnt);
}

void sendSDS011Info() {
  doc.clear();
  JsonObject sds011InfoJson = doc.createNestedObject("sds011_info");
  sprintf(buffer, "%d.%02d.%02d", sdsInfo.date.year, sdsInfo.date.month, sdsInfo.date.day);
  sds011InfoJson["firmware"] = buffer;
  sds011InfoJson["active"] = sdsInfo.active;
  sds011InfoJson["period"] = sdsInfo.period;
  sds011InfoJson["available"] = sdsInfo.available;
  sendWs("sendSDS011Info");
}

void sendOffsets() {
  doc.clear();

  JsonObject offsetsJson = doc.createNestedObject("offsets");

  offsetsJson["bmp180_temp"] = offsets.bmp180_temp;
  offsetsJson["bmp180_pressure"] = offsets.bmp180_pressure;
  offsetsJson["bme280_temp"] = offsets.bme280_temp;
  offsetsJson["bme280_humidity"] = offsets.bme280_humidity;
  offsetsJson["bme280_pressure"] = offsets.bme280_pressure;
  offsetsJson["dht20_temp"] = offsets.dht20_temp;
  offsetsJson["dht20_humidity"] = offsets.dht20_humidity;
  offsetsJson["tlv493d_temp"] = offsets.tlv493d_temp;
  offsetsJson["mhz19_temp"] = offsets.mhz19_temp;
  offsetsJson["mhz19_co2"] = offsets.mhz19_co2;
  offsetsJson["mix8410_u"] = offsets.mix8410_u;
  offsetsJson["multigas_no2"] = offsets.multigas_co;
  offsetsJson["multigas_c2h5oh"] = offsets.multigas_c2h5oh;
  offsetsJson["multigas_voc"] = offsets.multigas_voc;
  offsetsJson["multigas_co"] = offsets.multigas_co;
  offsetsJson["imu9dof_axe"] = offsets.imu9dof_axe;
  offsetsJson["imu9dof_aye"] = offsets.imu9dof_aye;
  offsetsJson["imu9dof_aze"] = offsets.imu9dof_aze;
  offsetsJson["imu9dof_gxe"] = offsets.imu9dof_gxe;
  offsetsJson["imu9dof_gye"] = offsets.imu9dof_gye;
  offsetsJson["imu9dof_gze"] = offsets.imu9dof_gze;
  offsetsJson["imu9dof_temp"] = offsets.imu9dof_temp;
  sendWs("sendOffsets");
}

void sendHAARP() {
  // Send magnet and gyro sensor history for HAARP detection
  doc.clear();

  JsonObject haarp = doc.createNestedObject("haarp");

  addBMP180Sensor((char *)"bmp180", haarp, curData->bmp180);
  addBME280Sensor((char *)"bme280", haarp, curData->bme280);
  addTLV493DSensor((char *)"tlv493d", haarp, curData->tlv493d);
  addIMU9DOFSensor((char *)"imu9dof", haarp, curData->imu9dof);

  sendWs("sendHAARP");
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;

  // While FTP client is connected to not respond to not overload WiFi stack.
  if (FtpConnected) {
    if (SerialOut) {
      Serial.println("W:WS Ignore -> FTP Client");
    }
    return;
  }

  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    char *s = (char *)data;

    data[len] = 0;
    if (SerialOut > 1) {
      Serial.print("I:WS Msg: ");
    }
    if (strcmp(s, "toggle_recording") == 0) {
      toggleRecording();
    } else if (strcmp(s, "toggle_debug_mode") == 0) {
      toggleDebugMode();
    } else if (strcmp(s, "toggle_serial_out") == 0) {
      toggleSerialOut();
    } else if (strcmp(s, "toggle_backlight") == 0) {
      toggleBacklight();
    } else if (strncmp(s, "get_history ", 12) == 0) {
      sendHistory((char *)&data[12]);
    } else if (strcmp(s, "clear_history") == 0) {
      clearHistory();
      sendOk(s);
    } else if (strcmp(s, "clear_max") == 0) {
      clearMax();
      sendOk(s);
    } else if (strcmp(s, "get_data") == 0) {
      sendDataWs(true);
    } else if (strcmp(s, "get_sds011_info") == 0) {
      sendSDS011Info();
    } else if (strcmp(s, "get_offsets") == 0) {
      sendOffsets();
    } else if (strcmp(s, "get_haarp") == 0) {
      sendHAARP();
    } else if (strncmp(s, "set_offsets ", 12) == 0) {
      // Syntax: set_offsets 0.0,-1.1,0.9,... Last value is an integer!
      char *p = (char *)&data[12];

      // BMP180
      offsets.bmp180_temp = atof(p);
      p = strchr(p, ',');
      if (p == NULL) return;
      offsets.bmp180_pressure = atof(p + 1);
      // BME280
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.bme280_temp = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.bme280_pressure = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.bme280_humidity = atof(p + 1);
      // DHT20
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.dht20_temp = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.dht20_humidity = atof(p + 1);
      // TLV493D
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.tlv493d_temp = atof(p + 1);
      // MHZ19B
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.mhz19_temp = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.mhz19_co2 = atof(p + 1);
      // MIX8410
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.mix8410_u = atof(p + 1);
      // MultiGas
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.multigas_no2 = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.multigas_c2h5oh = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.multigas_voc = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.multigas_co = atof(p + 1);
      // IMU9DOF
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_axe = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_aye = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_aze = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_gxe = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_gye = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_gze = atof(p + 1);
      p = strchr(p + 2, ',');
      if (p == NULL) return;
      offsets.imu9dof_temp = atof(p + 1);
      writeOffsets();
      clearHistory();
      sendOk((char *)"set_offsets");
      if (SerialOut > 1) {
        Serial.println("I:set_offsets OK");
      }
    } else if (strncmp(s, "list_sdcard_files ", 18) == 0) {
      char *p = (char *)&data[18];
      getGetSdCardContents(ws, atoi(p));
    } else if (strcmp(s, "get_memory_status") == 0) {
      sendMemoryStatus();
    } else if (strcmp(s, "reset_sensors") == 0) {
      ResetSensors = true;
    } else if (strcmp(s, "restart") == 0) {
      ESP.restart();
    } else {
      doc.clear();
      doc["error"] = "Unknown command";
      doc["command"] = s;
      sendWs("Unknown command");
    }
  }
}

bool sendSdCardContentsChunk(uint32_t nr, uint32_t cnt, String &fileNames, AsyncWebSocket &ws) {
  doc.clear();
  doc["filename_nr"] = nr - cnt;
  doc["filenames"] = fileNames;
  sprintf(buffer, "getGetSdCardContents %d %d", nr, cnt);
  return sendWs(buffer);
}

void addDate(JsonObject &obj, ms_date_t date) {
  JsonObject dateJson = obj.createNestedObject("date");
  dateJson["year"] = date.year;
  dateJson["month"] = date.month;
  dateJson["day"] = date.day;
}

void addGpsData(char *key, gps_t &gps, String &gpsData) {
  JsonObject obj = doc.createNestedObject(key);

  sprintf(buffer, "%d.%02d.%02d", gps.date.year, gps.date.month, gps.date.day);
  obj["date"] = buffer;
  sprintf(buffer, "%02d:%02d:%02d", gps.time.hour, gps.time.minute, gps.time.second);
  obj["time"] = buffer;
  obj["sat"] = gps.satellites;
  obj["lat"] = gps.latitude;
  obj["lng"] = gps.longitude;
  obj["alt"] = gps.altitude;
  obj["speed"] = gps.speed;
  obj["angle"] = gps.angle;
  obj["hdop"] = gps.hdop;
  obj["vdop"] = gps.vdop;
  obj["pdop"] = gps.pdop;
  obj["valid"] = gps.valid;
  obj["quality"] = gps.quality;
  obj["quality_3d"] = gps.quality_3d;
  if (DebugMode) {
    obj["raw_data"] = gpsData;
  }
}

bool sendHistoryItem(char *key, int16_t *array, uint8_t idx) {
  doc.clear();  

  JsonObject history = doc.createNestedObject("history");

  addArray(key, history, array, min((counter - ws_hist_counter[idx]), HISTORY_SIZE));
  ws_hist_counter[idx] = counter;
  return sendWs("sendDataWsHist");
}

void sendHistory(String key) {
  if (key == "all") {
    doc.clear();
    JsonObject history = doc.createNestedObject("history");
    addArray((char *)"temperature", history, histTemp, min(counter - ws_hist_counter[0], 10U));
    addArray((char *)"humidity", history, histHumidity, min(counter - ws_hist_counter[1], 10U));
    addArray((char *)"pressure", history, histPressure, min(counter - ws_hist_counter[2], 10U));
    addArray((char *)"altitude", history, histAltitude, min(counter - ws_hist_counter[3], 10U));
    addArray((char *)"co2", history, histCO2, min(counter - ws_hist_counter[4], 10U));
    addArray((char *)"o2", history, histO2, min(counter - ws_hist_counter[5], 10U));
    addArray((char *)"no2", history, histNO2, min(counter - ws_hist_counter[6], 10U));
    addArray((char *)"c2h5oh", history, histC2H5OH, min(counter - ws_hist_counter[7], 10U));
    addArray((char *)"voc", history, histVOC, min(counter - ws_hist_counter[8], 10U));
    addArray((char *)"co", history, histCO, min(counter - ws_hist_counter[9], 10U));
    addArray((char *)"pm25", history, histPM25, min(counter - ws_hist_counter[10], 10U));
    addArray((char *)"pm10", history, histPM10, min(counter - ws_hist_counter[11], 10U));
    for (int i = 0; i < 12; i++) {
      ws_hist_counter[i] = counter;
    }
    if (!sendWs("sendDataWsHist")) return;
  } else if (key == "temperature") {
    sendHistoryItem((char *)"temperature", histTemp, 0);
  } else if (key == "humidity") {
    sendHistoryItem((char *)"humidity", histHumidity, 1);
  } else if (key == "pressure") {
    sendHistoryItem((char *)"pressure", histPressure, 2);
  } else if (key == "altitude") {
    sendHistoryItem((char *)"altitude", histAltitude, 3);
  } else if (key == "co2") {
    sendHistoryItem((char *)"co2", histCO2, 4);
  } else if (key == "o2") {
    sendHistoryItem((char *)"o2", histO2, 5);
  } else if (key == "no2") {
    sendHistoryItem((char *)"no2", histNO2, 6);
  } else if (key == "c2h5oh") {
    sendHistoryItem((char *)"c2h5oh", histC2H5OH, 7);
  } else if (key == "voc") {
    sendHistoryItem((char *)"voc", histVOC, 8);
  } else if (key == "co") {
    sendHistoryItem((char *)"co", histCO, 9);
  } else if (key == "pm25") {
    sendHistoryItem((char *)"pm25", histPM25, 10);
  } else if (key == "pm10") {
    sendHistoryItem((char *)"pm10", histPM10, 11);
  }
}

void sendDataWs(bool force)
{
  if (!force && counter == ws_counter) {
    return;
  }
  if (SerialOut) {
    sprintf(buffer, "I:sendDataWs: force=%d", force);
    Serial.println(buffer);
    sprintf(buffer, "I:FREE=%d %d %d %d", esp_get_free_heap_size(), esp_get_minimum_free_heap_size(), ws.availableForWriteAll(), ws.count());
    Serial.println(buffer);
  }
  doc.clear();
  JsonObject headerJson = doc.createNestedObject("header");
  headerJson["startup_cnt"] = startup_cnt;
  headerJson["counter"] = counter;
  headerJson["data_cnt"] = data_cnt;
  headerJson["uptime"] = millis() / 1000 - start_time;
  headerJson["bl"] = BacklightEnabled;
  headerJson["rssi"] = WiFiRSSI;
  headerJson["serial"] = SerialOut;
  headerJson["recording"] = RecordingEnabled;

  addDate(headerJson, curData->date);

  JsonObject timeJson = headerJson.createNestedObject("time");
  timeJson["hour"] = curData->time.hour;
  timeJson["minute"] = curData->time.minute;
  timeJson["second"] = curData->time.second;

  JsonObject bmp180Json = doc.createNestedObject("bmp180");
  bmp180Json["temp"] = curData->bmp180.temp;

  JsonObject bme280Json = doc.createNestedObject("bme280");
  bme280Json["temp"] = curData->bme280.temp;
  bme280Json["humidity"] = curData->bme280.humidity;

  JsonObject dht20Json = doc.createNestedObject("dht20");
  dht20Json["temp"] = curData->dht20.temp;
  dht20Json["humidity"] = curData->dht20.humidity;

  JsonObject tlv493dJson = doc.createNestedObject("tlv493d");
  tlv493dJson["temp"] = curData->tlv493d.temp;

  JsonObject imu9dofJson = doc.createNestedObject("imu9dof");
  imu9dofJson["ak09918_error"] = curData->imu9dof.ak09918_error;
  imu9dofJson["temp"] = curData->imu9dof.temp;

  JsonObject mix8410Json = doc.createNestedObject("mix8410");
  mix8410Json["u"] = curData->mix8410.u;

  JsonObject multiGasJson = doc.createNestedObject("multigas");
  multiGasJson["no2"] = curData->multigas.no2;
  multiGasJson["c2h5oh"] = curData->multigas.c2h5oh;
  multiGasJson["voc"] = curData->multigas.voc;
  multiGasJson["co"] = curData->multigas.co;

  JsonObject mhz19Json = doc.createNestedObject("mhz19");
  mhz19Json["co2"] = curData->mhz19.co2;
  mhz19Json["temp"] = curData->mhz19.temp;
  mhz19Json["error"] = curData->mhz19.error;
  mhz19Json["version"] = mhz19_version;

  sds011_t sds011 = curData->sds011;
  JsonObject sds011Json = doc.createNestedObject("sds011");
  sds011Json["valid"] = sds011.valid;
  if (sds011.valid) {
    sds011Json["pm25"] = sds011.pm25;
    sds011Json["pm10"] = sds011.pm10;
  } else {
    sds011Json["error"] = SDS011Error;
  }

  addGpsData((char *)"pa1010d", curData->pa1010d, pa1010dData);

  JsonObject sdcardJson = doc.createNestedObject("sdcard");
  sdcardJson["type"] = sdInfo.card_type;
  sdcardJson["size"] = sdInfo.card_size;
  sdcardJson["total"] = sdInfo.total_bytes;
  sdcardJson["used"] = sdInfo.used_bytes;
  sdcardJson["file_cnt"] = sdInfo.file_cnt;
  sdcardJson["filename"] = fileName;

  JsonObject memJson = doc.createNestedObject("mem");
  memJson["free_heap_size"] = esp_get_free_heap_size();
  memJson["int_free_heap_size"] = esp_get_free_internal_heap_size();
  memJson["min_free_heap_size"] = esp_get_minimum_free_heap_size();

  if (!sendWs("sendDataWs")) return;

  ws_counter = counter;
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
             void *arg, uint8_t *data, size_t len) {
  switch (type) {
    case WS_EVT_CONNECT:
      if (SerialOut) {
        Serial.printf("WS client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      }
      sendDataWs(true);
      break;
    case WS_EVT_DISCONNECT:
      if (SerialOut) {
        Serial.printf("WS client #%u disconnected\n", client->id());
      }
      break;
    case WS_EVT_DATA:
      if (SerialOut) {
        Serial.printf("WS client #%u DATA\n", client->id());
      }
      handleWebSocketMessage(arg, data, len);
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
}

boolean setupWeb() {
  if (SerialOut) {
    Serial.println("I:setupWeb");
  }
  // First scan the network
  WiFiScanning = true;
  updateStatusBar(true);
  int n = WiFi.scanNetworks();
  WiFiScanning = false;
  for (int i = 0; i < n; i++) {
    String ssid = WiFi.SSID(i);

    if (SerialOut) {
      Serial.println("I:SSID " + ssid);
    }
    if (ssid.equals(ssid1)) {
      Serial.println("I:Connect...");
      WiFi.begin(ssid1, password1);
      return true;
    }
    if (ssid.equals(ssid2)) {
      Serial.println("I:Connect...");
      WiFi.begin(ssid2, password2);
      return true;
    }
  }
  WiFiEnabled = false;
  updateStatusBar(true);
  return false;
}

void setupWebSocket() {
  if (SerialOut) {
    Serial.println("I:setupWebSocket");
  }
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

bool updateWifiConnectionState() {
  Serial.print("ST=");
  Serial.print(WiFi.status());
  Serial.print("  SSID=");
  Serial.print(WiFi.SSID());
  Serial.print("  RSSI=");
  Serial.print(WiFi.RSSI());
  Serial.print("  IP=");
  Serial.println(WiFi.localIP());
  if (WiFi.status() != WL_CONNECTED) {
    if (WiFiStatus == WL_CONNECTED) {
      server.end();
      if (SerialOut) {
        Serial.println("I:WiFi disconnected.");
      }
    }
    WiFiStatus = WiFi.status();
    return false;
  }
  if (WiFiStatus != WL_CONNECTED) {
    if (SerialOut) {
      sprintf(buffer, "I:WiFi connected. IP address: %s", WiFi.localIP().toString().c_str());
      Serial.println(buffer);
    }
    WiFiStatus = WL_CONNECTED;
    initClock();
    sprintf(buffer, "IP: %s", WiFi.localIP().toString().c_str());
    showMessage(buffer);
    setupFtpServer();
    setupWebSocket();
      // Route for root / web page
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
      request->send(SD, "/web/index.html", "text/html");
    });
    server.on("/main.css", HTTP_GET, [](AsyncWebServerRequest *request) {
      request->send(SD, "/web/main.css", "text/css");
    });
    server.on("/favicon.png", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(SD, "/web/favicon.png", "image/png");
    });
    server.on("/main.js", HTTP_GET, [](AsyncWebServerRequest *request) {
      request->send(SD, "/web/main.js", "text/javascript");
    });
    server.on("/multisensor.js", HTTP_GET, [](AsyncWebServerRequest *request) {
      request->send(SD, "/web/multisensor.js", "text/javascript");
    });
    server.begin();
  }
  return true;
}

bool updateWiFiStatus() {
  if (!WiFiEnabled) {
    return false;
  }
  if (!updateWifiConnectionState()) {
    return false;
  }
  WiFiRSSI = WiFi.RSSI();
  return true;
}

void toggleWiFiStatus() {
  WiFiEnabled = !WiFiEnabled;
  if (SerialOut) {
    sprintf(buffer, "I:toggleWiFiStatus %d", WiFiEnabled);
    Serial.println(buffer);
  }
  if (WiFiEnabled) {
    if (!setupWeb()) {
      setupWeb();
    }
  } else {
    WiFi.disconnect(true);
    WiFi.mode(WIFI_OFF);
  }
  updateWiFiStatus();
}

void resetSensors() {
  uint8_t result;

  doc.clear();
  resetBMP180();
  resetBME280();
  result = resetIMU9DOF();
  if (result != 0) {
    doc["imu9dof"] = result;
  }
  doc["result"] = "reset_sensors";
  sendWs("reset_sensors");
}

void handleWebServer(bool force) {
  if (ResetSensors) {
    resetSensors();
    ResetSensors = false;
  }
  if (!WiFiEnabled) {
    return;
  }
  ws.cleanupClients(2); // Max 2 clients
  if (ws.count() == 0) {
    return;
  }
  if (!FtpConnected) {
    sendDataWs(force);
    sendHAARP();
  }
}

#endif // _WEB_H
