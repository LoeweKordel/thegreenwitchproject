#ifndef _FLOAT16_H
#define _FLOAT16_H

float f16tof32(uint16_t value) {
    uint16_t sgn = (value & 0x8000) > 0;

    // ZERO
    if ((value & 0x7FFF) == 0) {
        return sgn ? -0 : 0;
    }
    // NAN & INF
    int exp = (value & 0x7C00) >> 10;
    uint16_t man = value & 0x03FF;

    if (exp == 0x001F) {
        if (man == 0) return sgn ? -INFINITY : INFINITY;
        else return NAN;
    }

    // SUBNORMAL/NORMAL
    float f;

    if (exp == 0) f = 0.0;
    else          f = 1.0;

    // PROCESS MANTISSE
    for (int i = 9; i >= 0; i--) {
        f *= 2.0;
        if (man & (1 << i)) f += 1.0;
    }
    f *= pow(2.0, exp - 25);
    if (exp == 0) {
        f *= pow(2.0, -13);    // 5.96046447754e-8;
    }
    return sgn ? -f : f;
}

uint16_t f32tof16(float f) {
    uint32_t t = *(uint32_t *) &f;
    // man bits = 10; but we keep 11 for rounding
    uint16_t man = (t & 0x007FFFFF) >> 12;
    int16_t  exp = (t & 0x7F800000) >> 23;
    bool     sgn = (t & 0x80000000);

    // handle 0
    if ((t & 0x7FFFFFFF) == 0) {
        return sgn ? 0x8000 : 0x0000;
    }
    // denormalized float32 does not fit in float16
    if (exp == 0x00) {
        return sgn ? 0x8000 : 0x0000;
    }
    // handle infinity & NAN
    if (exp == 0x00FF) {
        if (man) return 0xFE00;         //  NAN
        return sgn ? 0xFC00 : 0x7C00;   // -INF : INF
    }
    // normal numbers
    exp = exp - 127 + 15;
    // overflow does not fit => INF
    if (exp > 30) {
        return sgn ? 0xFC00 : 0x7C00;   // -INF : INF
    }
    //  subnormal numbers
    if (exp < -38) {
        return sgn ? 0x8000 : 0x0000;  // -0 or 0  ?   just 0 ?
    }
    if (exp <= 0) { // subnormal
        man >>= (exp + 14);
        // rounding
        man++;
        man >>= 1;
        if (sgn) return 0x8000 | man;
        return man;
    }

    // normal
    // TODO rounding
    exp <<= 10;
    man++;
    man >>= 1;
    if (sgn) return 0x8000 | exp | man;
    return exp | man;
}

#endif // _FLOAT16_H
