// Grove - Gas Sensor(O2) test code
// Note:
// 1. It need about about 5-10 minutes to preheat the sensor
// 2. modify VRefer if needed

#ifndef _MIX8410_H
#define _MIX8410_H

#include "common.h"

// voltage of adc reference
#define ADC_REF 3.3

#define MIX8410_PIN 36


bool setupMIX8410(int16_t y) {
  analogSetPinAttenuation(MIX8410_PIN, ADC_11db);
  return true;
}

float readO2U() {
  long sum = 0;

  for(int i = 0; i < 32; i++) {
    sum += analogRead(MIX8410_PIN);
  }
  return (sum >> 5) * (ADC_REF / 4095.0);
}

float calcO2Percent(float u) {
  return u * 21.0 / 2.0;
}

void updateMIX8410(mix8410_t *p) {
  p->u = readO2U();
}
 
#endif // _MIX8410_H
