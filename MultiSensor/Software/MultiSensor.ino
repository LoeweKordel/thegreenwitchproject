//-----------------------------------------------------
// MultiSensor
// Autor:   Martin Bammer
// License: GNU GPl 3.0
// Created: 21. Dec 2022
//-----------------------------------------------------
// Board: uPesy ESP32 WROOM DevKit

#include <SPI.h>
#include <Wire.h>

#include "common.h"
#include "dht20.h"
#include "bmp180.h"
#include "bme280.h"
#include "tlv493d.h"
#include "sds011.h"
#include "mhz19.h"
#include "mix8410.h"
#include "imu9dof.h"
#include "multigas.h"
#include "pa1010d.h"
#include "sd.h"
#include "web.h"
#include "clock.h"
#include "eeprom.h"
#include "display.h"
#include "button.h"
#include "ftpserver.h"

/*
PM10: Der Tagesgrenzwert beträgt 50 µg/m3 und darf nicht öfter als 35mal im Jahr überschritten werden.
      Der zulässige Jahresmittelwert beträgt 40 µg/m3.
PM2.5: Der Tagesgrenzwert beträgt 25 µg/m3 und darf nicht öfter als 35mal im Jahr überschritten werden.
       Der zulässige Jahresmittelwert beträgt 20 µg/m3.

V1.0
====

I2C address table:
  0x0D HMC5883L
  0x38 DHT20
  0x50 EEPROM
  0x57 24C32
  0x5E TLV493D (Alt. 0x1F)
  0x68 DS1307 (TinyRTC)
  0x69 MPU6050 (=GY521), AD0=HIGH, (Alt. 0x68)
  0x76 BME680 (Alt. 0x77)
  0x77 BMP180

UART:
  CO2 MHZ19C
  PMx SDS011


V2.0
====

I2C address table:
  0x08 Seeed Multigas Sensor (CO, NO2, C2H5CH, VOC)
  0x0C Grove IMU 9DOF (AK09918: Compass)
  0x10 Adafruit Mini GPS PA1010D
  0x38 DHT20 (Temp, Humi)
  0x50 EEPROM
  0x57 24C32
  0x5E TLV493D (Alt. 0x1F)
  -- PLANNED: 0x68 PCF8523 (RTC) --
  0x68 DS1307 (TinyRTC)
  0x69 Grove IMU 9DOF (ICM20600: Accel, Gyro)
  0x76 BME280 (Press, Temp, Humi)
  0x77 BMP180 (Press, Temp)

UART:
  CO2 MHZ19C
  PMx SDS011

*/

// Some definitions
#define UPDATETIME  5000

int16_t setup_y = 0;

unsigned long nextSensorUpdate = 0;
unsigned long next50msUpdate = 0;
unsigned long next100msUpdate = 0;
unsigned long next500msUpdate = 0;

bool setupCore(int16_t y) {
  setupEEPROM();
  setupClock();
  setupButton();
  return true;
}

void setupSensor(String name, bool (*func)(int16_t y)) {
  Serial.print("I:Init " + name + "... ");
  tft.setTextColor(TFT_WHITE);
  tft.setCursor(10, setup_y);
  tft.print("Init ");
  tft.print(name);
  tft.setCursor(212, setup_y);
  if (func(setup_y)) {
    tft.setTextColor(TFT_GREEN);
    tft.setCursor(212, setup_y);
    tft.println("DONE");
    Serial.println("I:DONE");
  } else {
    tft.setTextColor(TFT_RED);
    tft.setCursor(212, setup_y);
    tft.println("FAIL");
    Serial.println("E:FAIL");
  }
  setup_y += 20;
}

void showSdCardInfo() {
  tft.fillScreen(TFT_BLACK); // Clear screen
  tft.setTextColor(TFT_WHITE);
  showInfoLine(20, (char *)"SD Card Type", getSdCardType());
  sprintf(buffer, "%.1f GB", (float)sdInfo.card_size / GB);
  showInfoLine(40, (char *)"Card Size", buffer);
  sprintf(buffer, "%.1f GB", (float)sdInfo.total_bytes / GB);
  showInfoLine(60, (char *)"Volume Size", buffer);
  sprintf(buffer, "%.1f MB", (float)sdInfo.used_bytes / MB);
  showInfoLine(80, (char *)"Used Size", buffer);
  sprintf(buffer, "%d", sdInfo.file_cnt);
  showInfoLine(100, (char *)"File Cnt", buffer);
  sprintf(buffer, "%d", startup_cnt);
  showInfoLine(120, (char *)"Start Cnt", buffer);
  sprintf(buffer, "%.1f h", (float)operating_time / 60.0);
  showInfoLine(140, (char *)"Oper. Time", buffer);
}

void printDeviceInfo(char* swVersion) {
  Serial.println("I:MultiSensor");
  Serial.println("I:(c) Martin Bammer");
  sprintf(tmpbuffer, "I:HwVersion=%.1f", float(HW_VERSION) * 0.1);
  Serial.println(tmpbuffer);
  sprintf(tmpbuffer, "I:SwVersion=%s", swVersion);
  Serial.println(tmpbuffer);
  sprintf(buffer, "I:DataVersion=%.1f", float(DATA_VERSION) * 0.1);
  Serial.println(buffer);
  sprintf(buffer, "I:SerialNr=%d", SERIAL_NR);
  Serial.println(buffer);
  sprintf(buffer, "I:SD Card Type=%d", sdInfo.card_type);
  Serial.println(buffer);
  sprintf(buffer, "I:SD Card Size=%d MB", (uint32_t)sdInfo.card_size / MB);
  Serial.println(buffer);
  sprintf(buffer, "I:SD Volume Size=%d MB", (uint32_t)sdInfo.total_bytes / MB);
  Serial.println(buffer);
  sprintf(buffer, "I:SD Used Size=%d MB", (uint32_t)sdInfo.used_bytes / MB);
  Serial.println(buffer);
  sprintf(buffer, "I:SD File Cnt=%d", sdInfo.file_cnt);
  Serial.println(buffer);
}

void setup() {
  Serial.begin(230400);
  while (!Serial) delay(10);    // Wait until serial communication is ready

  setupDisplay();
  //Start with some text in serial monitor and tft
  tft.setTextSize(3);
  tft.setTextColor(TFT_RED);
  tft.setCursor(45, 80);
  tft.print("Multi");
  tft.setTextColor(TFT_GREEN);
  tft.print("Sensor");
  tft.setTextSize(2);
  tft.setTextColor(TFT_ORANGE);
  tft.setCursor(78, 110);
  sprintf(tmpbuffer, "Version %.1f", float(SW_VERSION) * 0.1);
  tft.print(tmpbuffer);
  tft.setTextColor(TFT_YELLOW);
  tft.setCursor(40, 130);
  tft.print("(c) Martin Bammer");
  delay(2000);

  // Dynamically allocate buffers
  data = (data_t *)calloc(1, DATA_SIZE * sizeof(data_t));
  memset(data, 0, DATA_SIZE * sizeof(data_t));
  curData = &data[0];

  tft.fillScreen(TFT_BLACK); // Clear screen
  // Initialize sensors
  setupSensor("Core", setupCore);
  setupSensor("SD-Card", setupSDCard);
  setupSensor("DHT20", setupDHT20);
  setupSensor("BMP180", setupBMP180);
  setupSensor("BME280", setupBME280);
  setupSensor("TLV493D", setupTLV493D);
  setupSensor("SDS011", setupSDS011);
  setupSensor("MHZ19", setupMHZ19);
  setupSensor("MIX8410", setupMIX8410);
  setupSensor("IMU9DOF", setupIMU9DOF);
  setupSensor("MultiGas", setupMultiGas);
  setupSensor("PA1010D", setupPA1010D);

  // Check if known WiFi network is in range.
  toggleWiFiStatus();
  // Wait for "." on serial port to activate Serial debug mode
  Serial.println("I:Wait for input (s, d)...");
  bool KeepSerialOut = false;
  for (uint8_t i = 0; i < 70; i++) {
    if (i == 20) {
      showSdCardInfo();
    }
    if (Serial.available()) {
      char c = Serial.read();

      if (c == 's') {
        KeepSerialOut = true;
        break;
      } else if (c == 'd') {
        DisplayAlwaysOn = true;
        break;
      }
    }
    delay(100);
  }
  updateWiFiStatus();
  if (WiFiStatus == WL_CONNECTED) {
    // If yes keep WiFi and display on
    DisplayAlwaysOn = true;
    Serial.println("I:Connected. Activate Home Mode.");
  } else {
    // If not then switch WiFi off again.
    toggleWiFiStatus();
    Serial.println("I:No known network.");
  }
  Serial.println("I:Starting...");

  tft.fillScreen(TFT_BLACK); // Clear screen
  updateStatusBar(true);
  updateUser(true);

  xTaskCreatePinnedToCore(
      sds011Task,   // Display task
      "sds011Task", // Task name
      2048,       // Stack size
      NULL,       // No parameters
      1,          // Priority
      NULL,       // No handle returned
      0);         // CPU 1

  if (SerialOut) {
    printDeviceInfo(tmpbuffer);
  }
  if (!KeepSerialOut) {
    SerialOut = 0;
  }
}

void loop() {
  unsigned long time = millis();

  if (Serial.available()) {
    char c = Serial.read();

    if (c == '?') {
      Serial.println("\nMenu:");
      Serial.println("s=SerialOut on/off");
      Serial.println("d=Display on/off");
      Serial.println("r=Recording on/off");
      Serial.println("w=WiFi on/off");
      Serial.println("t=Timing on/off");
      Serial.println("l=List SD-Card contents");
      Serial.println("f<Name>=Get BIN file");
      Serial.println("F<Name>=Delete BIN file");
      Serial.println("R=Reset/Reinit some sensors");
      Serial.println("D=Delete up to 100 data files");
    } else if (c == 's') {
      SerialOut = (SerialOut + 1) % 3;
      sprintf(buffer, "\n::Serial=%d", SerialOut);
      Serial.println(buffer);
    } else if (c == 'd') {
      DisplayAlwaysOn = !DisplayAlwaysOn;
      setBacklight(true);
      sprintf(buffer, "\n::Display=%d", DisplayAlwaysOn);
      Serial.println(buffer);
    } else if (c == 'r') {
      RecordingEnabled = !RecordingEnabled;
      sprintf(buffer, "\n::Recording=%d", RecordingEnabled);
      Serial.println(buffer);
    } else if (c == 'w') {
      toggleWiFiStatus();
      sprintf(buffer, "\n::WiFi=%d", WiFiEnabled);
      Serial.println(buffer);
    } else if (c == 't') {
      ShowTiming = !ShowTiming;
      sprintf(buffer, "\n::ShowTiming=%d", ShowTiming);
      Serial.println(buffer);
    } else if (c == 'l') {
      listGetSdCardContents();
    } else if (c == 'f') {
      getBinFile();
    } else if (c == 'F') {
      deleteBinFile();
    } else if (c == 'R') {
      Serial.println("::Reset sensors");
      setupPA1010D(0);
    } else if (c == 'D') {
      Serial.println("::Delete 100 data files");
      sprintf(buffer, "::hasMore=%d", deleteDataFiles(100));
      Serial.println(buffer);
    }
  }
  if (time > nextSensorUpdate) {
    nextSensorUpdate = time + UPDATETIME; // Update time to get data after 60sec
    if (SerialOut > 1) {
      sprintf(buffer, "Update iteration %d/%d", data_cnt, DATA_SIZE);
      Serial.println(buffer);
    }
    handleWebServer(true);
    if (data_cnt >= DATA_SIZE) {
      data_cnt = 0;
      memset(data, 0, DATA_SIZE * sizeof(data_t));
    }

    data_t *p = &data[data_cnt];

    curData = p;
    p->counter = counter;
    p->date.year = clockInfo.year;
    p->date.month = clockInfo.month;
    p->date.day = clockInfo.day;
    p->time.hour = clockInfo.hour;
    p->time.minute = clockInfo.minute;
    p->time.second = clockInfo.second;
    memcpy(&p->sds011, &sdsData, sizeof(sdsData));
    updateBMP180(&p->bmp180);
    updateBME280(&p->bme280);
    updateDHT20(&p->dht20);
    updateTLV493D(&p->tlv493d);
    updateMHZ19(&p->mhz19);
    updateMIX8410(&p->mix8410);
    updateMultiGas(&p->multigas);

    float temp = getAvgTemperature();
    float humidity = getAvgHumidity();

    displayInfo.data_cnt = data_cnt + 1;
    displayInfo.temp = temp;
    displayInfo.temp_max = getMaxTemperature();
    displayInfo.temp_min = getMinTemperature();
    displayInfo.humidity = humidity;
    displayInfo.humidity_max = getMaxHumidity();
    displayInfo.humidity_min = getMinHumidity();
    displayInfo.pressure = getAvgPressure();
    displayInfo.pressure_max = getMaxPressure();
    displayInfo.pressure_min = getMinPressure();
    displayInfo.altitude = lround(calcAltitude(displayInfo.pressure));
    displayInfo.atm = displayInfo.pressure / 1.01325;
    displayInfo.co2 = (float)p->mhz19.co2 + offsets.mhz19_co2;
    displayInfo.co2_max = max(displayInfo.co2, displayInfo.co2_max);
    displayInfo.o2 = calcO2Percent(p->mix8410.u + offsets.mix8410_u);
    displayInfo.o2_max = max(displayInfo.o2, displayInfo.o2_max);
    displayInfo.no2 = getNO2ppm(p->multigas.no2, temp, humidity) + offsets.multigas_no2;
    displayInfo.no2_max = max(displayInfo.no2, displayInfo.no2_max);
    displayInfo.c2h5oh = getC2H5OHppm(p->multigas.c2h5oh, temp, humidity) + offsets.multigas_c2h5oh;
    displayInfo.c2h5oh_max = max(displayInfo.c2h5oh, displayInfo.c2h5oh_max);
    displayInfo.voc = getVOCppm(p->multigas.voc, temp, humidity) + offsets.multigas_voc;
    displayInfo.voc_max = max(displayInfo.voc, displayInfo.voc_max);
    displayInfo.co = getCOppm(p->multigas.co, temp, humidity) + offsets.multigas_co;
    displayInfo.co_max = max(displayInfo.co, displayInfo.co_max);
    displayInfo.pm_valid = p->sds011.valid;
    if (displayInfo.pm_valid) {
      displayInfo.pm25 = (float)p->sds011.pm25 / 10.0;
      displayInfo.pm25_max = max(displayInfo.pm25, displayInfo.pm25_max);
      displayInfo.pm10 = (float)p->sds011.pm10 / 10.0;
      displayInfo.pm10_max = max(displayInfo.pm10, displayInfo.pm10_max);
    }

    // Update history
    updateHistory(histTemp, displayInfo.temp, 10.0);
    updateHistory(histHumidity, displayInfo.humidity, 10.0);
    updateHistory(histPressure, displayInfo.pressure, 10.0);
    updateHistory(histAltitude, calcAltitude(displayInfo.pressure), 1.0);
    updateHistory(histCO2, displayInfo.co2, 10.0);
    updateHistory(histO2, displayInfo.o2, 10.0);
    updateHistory(histNO2, displayInfo.no2, 10.0);
    updateHistory(histC2H5OH, displayInfo.c2h5oh, 10.0);
    updateHistory(histVOC, displayInfo.voc, 10.0);
    updateHistory(histCO, displayInfo.co, 10.0);
    updateHistory(histPM25, displayInfo.pm25, 10.0);
    updateHistory(histPM10, displayInfo.pm10, 10.0);

    counter++;
    data_cnt++;
  }
  unsigned long tSensorUpdate = millis();
  updatePA1010D(&curData->pa1010d);
  displayInfo.pa1010d_sat = curData->pa1010d.satellites;
  displayInfo.pa1010d_alt = curData->pa1010d.altitude;
  displayInfo.pa1010d_lat = curData->pa1010d.latitude;
  displayInfo.pa1010d_lng = curData->pa1010d.longitude;
  if (curData->pa1010d.date.year >= 2023) {
    GpsAvailable = curData->pa1010d.satellites + 10;
  } else {
    GpsAvailable = 0;
  }
  unsigned long tPA1010D = millis();
  if (time > next50msUpdate) { // 20Hz
    next50msUpdate = time + 50;
    updateBMP180Pressure(&curData->bmp180);
    updateBME280Pressure(&curData->bme280);
    updateTLV493D(&curData->tlv493d);
    updateAK09918(&curData->imu9dof);
  }
  unsigned long t50ms = millis();
  if (time > next500msUpdate) { // 2Hz
    updateICM20600(&curData->imu9dof);
    updateClock(curData);
    updateSdCardStatus(false);
  }
  unsigned long t500ms = millis();
  if (time > next100msUpdate) { // 10Hz
    next100msUpdate = time + 100;
    updateButton();
    updateWiFiStatus();
  }
  unsigned long t100ms = millis();
  if (time > next500msUpdate) { // 2Hz
    next500msUpdate = time + 500;
    updateUser(false);
  }
  unsigned long tUser = millis();
  handleFtpServer();
  if (ShowTiming) {
    unsigned long dt = tSensorUpdate - time;
    if (dt > 10) {
      sprintf(buffer, "dtSensorUpdate=%lu", dt);
      Serial.println(buffer);
    }
    dt = tPA1010D - tSensorUpdate;
    if (dt > 10) {
      sprintf(buffer, "dtPA1010D=%lu", dt);
      Serial.println(buffer);
    }
    dt = t50ms - tPA1010D;
    if (dt > 10) {
      sprintf(buffer, "dt50ms=%lu", dt);
      Serial.println(buffer);
    }
    dt = t500ms - t50ms;
    if (dt > 10) {
      sprintf(buffer, "dt500ms=%lu", dt);
      Serial.println(buffer);
    }
    dt = t100ms - t500ms;
    if (dt > 10) {
      sprintf(buffer, "dt100ms=%lu", dt);
      Serial.println(buffer);
    }
    dt = tUser - t100ms;
    if (dt > 10) {
      sprintf(buffer, "dtUser=%lu", dt);
      Serial.println(buffer);
    }
  }
}
