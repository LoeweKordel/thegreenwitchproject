#ifndef _BUTTON_H
#define _BUTTON_H

#include <esp32-hal-gpio.h>

#include "common.h"

#define BUTTON_PIN 15

bool oldButtonStatus = true;
unsigned long buttonDownTime = 0;

void setupButton() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  oldButtonStatus = digitalRead(BUTTON_PIN);
}

void updateButton() {
  bool buttonStatus = digitalRead(BUTTON_PIN);

  if (backlight_off > 0 && backlight_off < 255) {
    backlight_off--;
    if (backlight_off == 0) {
      setBacklight(DisplayAlwaysOn);
    }
  }
  if (!buttonStatus) {
    // Button down
    if (oldButtonStatus) {
      buttonDownTime = millis();
      if (SerialOut) {
        Serial.println("I:Button Down");
      }
      oldButtonStatus = buttonStatus;
    } else {
      unsigned long dt = millis() - buttonDownTime;

      if (dt > 1200) {
        if (MenuPos == 255) {
          MenuPos = 254;
          UpdateMenu = true;
        } else if (dt < 1600) {
          showMenu(true);
        }
      }
    }
  } else if (buttonStatus && !oldButtonStatus) {
    // Button up
    unsigned long dt = millis() - buttonDownTime;
    if (SerialOut) {
      sprintf(buffer, "I:Button Up %lu", dt);
      Serial.println(buffer);
    }
    backlight_off = BACKLIGHT_OFF_TIME;
    if (dt <= 500) {
      if (MenuPos < 255) {
        MenuPos = (MenuPos + 1) % 7;
        UpdateMenu = true;
        updateUser(true);
      } else if (!BacklightEnabled) {
        setBacklight(true);
      } else {
        DisplayPage = (enum DISP)((DisplayPage + 1) % CNT);
        clearScreen();
        updateDisplay(true);
      }
    } else if (dt > 1500) {
      if (MenuPos == 254) {
        MenuPos = 0;
        UpdateMenu = true;
      } else if (MenuPos < 254) {
        if (MenuPos == 1) {
          DisplayAlwaysOn = !DisplayAlwaysOn;
        } else if (MenuPos == 2) {
          toggleWiFiStatus();
        } else if (MenuPos == 3) {
          RecordingEnabled = !RecordingEnabled;
          counter = 0;
          data_cnt = 0;
          displayInfo.data_cnt = 0;
          if (SerialOut) {
            sprintf(buffer, "I:Recording %d", RecordingEnabled);
            Serial.println(buffer);
          }
        } else if (MenuPos == 4) {
          SerialOut = !SerialOut;
        } else if (MenuPos == 5) {
          clearMax();
        } else if (MenuPos == 6) {
          // Home Mode
          DisplayAlwaysOn = true;
          RecordingEnabled = false;
          toggleWiFiStatus();
        }
        MenuPos = 255;
        clearScreen();
        updateDisplay(true);
      }
    }
  }
  oldButtonStatus = buttonStatus;
}

#endif // _BUTTON_H
 
