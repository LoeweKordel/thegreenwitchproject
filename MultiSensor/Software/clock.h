#ifndef _CLOCK_H
#define _CLOCK_H

#include <time.h>
#include <sys/time.h>
#include <sys/select.h>
#include <WiFi.h>
#include <Wire.h> // must be included here so that Arduino library object file references work
#include <RtcDS1307.h>
#include <RtcDateTime.h>

#include "common.h"
#include "eeprom.h"

// DS1307 I2C address 0x68

/* Rtc.getLastError() possible error codes:
  0: success
  1: busy timeout upon entering endTransmission()
  2: START bit generation timeout
  3: end of address transmission timeout
  4: data byte transfer timeout
  5: data byte transfer succeeded, busy timeout immediately after
  6: timeout waiting for peripheral to clear stop bit
*/

unsigned long uptime_last_update_time = 0;

RtcDS1307<TwoWire> Rtc(Wire);

char* clockInfoToTimeString() {
  sprintf(buffer, " %02d:%02d:%02d", clockInfo.hour, clockInfo.minute, clockInfo.second);
  if (!clockInfo.valid) {
    buffer[0] = '!';
  }
  return buffer;
}

RtcDateTime getRtcDateTime() {
  RtcDateTime now = Rtc.GetDateTime();

  clockInfo.year = now.Year();
  clockInfo.month = now.Month();
  clockInfo.day = now.Day();
  clockInfo.hour = now.Hour();
  clockInfo.minute = now.Minute();
  clockInfo.second = now.Second();
  return now;
}

bool setSysDateTime() {
  struct tm localTime;
  timeval tv;
  timezone tz;

  if (clockInfo.year < 2023) return false;
  localTime.tm_year = clockInfo.year - 1900;
  if (clockInfo.month < 1) return false;
  localTime.tm_mon = clockInfo.month - 1;
  localTime.tm_mday = clockInfo.day;
  localTime.tm_hour = clockInfo.hour;
  localTime.tm_min = clockInfo.minute;
  localTime.tm_sec = clockInfo.second;
  tv.tv_sec = mktime(&localTime);
  tv.tv_usec = 0;
  tz.tz_minuteswest = 3600;
  tz.tz_dsttime = DST_NONE;
  settimeofday(&tv, &tz);
  return true;
}

time_t getSysDateTime() {
  struct tm localTime;
  time_t now;

  bool sysTimeValid = getLocalTime(&localTime, 100);
  time(&now);
  localtime_r(&now, &localTime);
  clockInfo.year = localTime.tm_year + 1900;
  clockInfo.month = localTime.tm_mon + 1;
  clockInfo.day = localTime.tm_mday;
  clockInfo.hour = localTime.tm_hour;
  clockInfo.minute = localTime.tm_min;
  clockInfo.second = localTime.tm_sec;
  if (sysTimeValid) return now;
  return 0;
}

void setupClock() {
  RtcDateTime minValid = RtcDateTime(725846400); // 2023.01.01 00:00:00
  RtcDateTime maxValid = RtcDateTime(1577836800); // 2050

  Rtc.Begin();
  if (Rtc.IsDateTimeValid()) {
    clockInfo.valid = true;
    clockInfo.error = 0;
  } else {
    clockInfo.valid = false;
    clockInfo.error = Rtc.LastError();
    if (clockInfo.error != 0) {
      // we have a communications error
      // see https://www.arduino.cc/en/Reference/WireEndTransmission for 
      // what the number means
      sprintf(buffer, "E:RTC communications error=%d", clockInfo.error);
      Serial.println(buffer);
    } else {
      Serial.println("E:RTC lost confidence in the DateTime!");
    }
  }
  if (!Rtc.GetIsRunning()) {
    Serial.println("W:RTC was not actively running, starting now");
    Rtc.SetIsRunning(true);
  }

  RtcDateTime now = getRtcDateTime();

  if (now < minValid || now > maxValid) {
    Serial.println("E:RTC is outside of valid range!");
    clockInfo.error = 0x7f;
  } else {
    setSysDateTime();
  }

  // never assume the Rtc was last configured by you, so
  // just clear them to your needed state
  Rtc.SetSquareWavePin(DS1307SquareWaveOut_Low); 
}

void initClock() {
  configTime(3600, 3600, "pool.ntp.org");
}

void updateClock(data_t *p) {
  time_t now = getSysDateTime();
  uint32_t rtcNow = Rtc.GetDateTime().Unix32Time();
  unsigned long dt;
  char *tz = getenv("TZ");

  if (tz != NULL && tz[0] != 0) {
    if (strlen(tz) > 6) {
      long offset = atoi(&tz[3]);
      rtcNow += offset * 3600;
    }
  }
  if (now > rtcNow) dt = now - rtcNow;
  else dt = rtcNow - now;
  if (now && (dt > 3610)) { // More than 1min?
    const RtcDateTime tm = RtcDateTime(clockInfo.year, clockInfo.month, clockInfo.day, clockInfo.hour, clockInfo.minute, clockInfo.second);

    sprintf(buffer, "I:Adjust RTC (now=%ld RTC=%d dt=%ld)", now, rtcNow, dt);
    Serial.println(buffer);
    Rtc.SetDateTime(tm);
  }

  unsigned long current_time = millis() / 1000;

  if (current_time - uptime_last_update_time > 1 && (current_time - start_time) % 300 == 0) {
    // Each tick is 5min
    uptime_last_update_time = current_time;
    operating_time++;
    updateOperatingTime();
  }
}

#endif // _CLOCK_H
