#ifndef _MULTIGAS_H
#define _MULTIGAS_H

#include <Multichannel_Gas_GMXXX.h>

#include <Wire.h>

GAS_GMXXX<TwoWire> multigas;

// GM102B (NO2)
// Rs means resistance of sensor in 2ppm NO2 under different temp. and humidity.
// Rso means resistance of the sensor in 2ppm NO2 under 20°C/55%RH.

const float gm102b_rh_offset[4][7] PROGMEM = {
  { -10.0, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0 }, // °C
  { 1.71, 1.58, 1.45, 1.39, 1.12, 1.00, 0.89 }, // Rs/R0 @ 30%RH
  { 1.49, 1.32, 1.28, 1.08, 0.99, 0.88, 0.71 }, // Rs/R0 @ 60%RH
  { 1.28, 1.15, 10.9, 0.90, 0.86, 0.71, 0.68 }  // Rs/R0 @ 85%RH
};

const float gm102b_u2gas[2][12] PROGMEM = {
  { 0.0, 0.21, 0.39, 0.7, 0.95, 1.15, 1.35, 1.45, 1.6, 1.69, 1.79, 1.81 }, // V
  { 0.0, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 }, // NO2 [ppm]
};

// GM302B (Ethanol=C2H5OH)
// Rs means resistance of sensor in 50ppm ethanol under different temp. and humidity.
// Rso means resistance of the sensor in 50ppm ethanol under 20°C/65%RH.

const float gm302b_rh_offset[4][13] PROGMEM = {
  { -10.0, -5.0, 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0 },  // °C
  { 1.71, 1.61, 1.58, 1.50, 1.42, 1.30, 1.25, 1.18, 1.15, 1.12, 1.00, 0.92, 0.88 }, // Rs/R0 @ 30%RH
  { 1.45, 1.36, 1.33, 1.28, 1.20, 1.11, 1.08, 1.00, 0.98, 0.95, 0.85, 0.79, 0.73 }, // Rs/R0 @ 60%RH
  { 1.27, 1.20, 1.18, 1.10, 1.05, 0.95, 0.92, 0.88, 0.86, 0.81, 0.72, 0.69, 0.64 }  // Rs/R0 @ 85%RH
};

const float gm302b_u2gas[2][11] PROGMEM = {
  { 1.25, 1.5, 2.0, 2.25, 2.5, 3.1, 3.3, 3.6, 3.7, 3.8, 3.85 }, // Alcohol/Ethanol [V]
  { 0.0, 1.0, 3.5, 5.0, 10.0, 30.0, 50.0, 80.0, 100.0, 200.0, 500.0 } // VOC [ppm]
};

// GM502B (VOC)
// Rs means resistance of sensor in 150ppm CO gas under different temp. and humidity.
// Rso means resistance of the sensor in 150ppm CO gas under 20°C/55%RH.

const float gm502b_rh_offset[4][13] PROGMEM = {
  { -10.0, -5.0, 0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0 },  // °C
  { 1.71, 1.62, 1.54, 1.50, 1.42, 1.30, 1.25, 1.16, 1.14, 1.11, 1.00, 0.92, 0.88 }, // Rs/R0 @ 30%RH
  { 1.45, 1.38, 1.35, 1.28, 1.21, 1.11, 1.08, 1.00, 0.98, 0.96, 0.85, 0.79, 0.75 }, // Rs/R0 @ 60%RH
  { 1.25, 1.20, 1.18, 1.10, 1.05, 0.95, 0.92, 0.88, 0.86, 0.81, 0.73, 0.68, 0.62 }  // Rs/R0 @ 85%RH
};

const float gm502b_u2gas[2][9] PROGMEM = {
  { 2.52, 2.90, 3.20, 3.40, 3.60, 3.90, 4.05, 4.15, 4.20 }, // Alcohol [V]
  { 0.0, 1.0, 3.5, 5.0, 10.0, 30.0, 50.0, 80.0, 100.0 }  // VOC [ppm]
};

// GM702B (CO)
// Rs means resistance of sensor in 150ppm CO gas under different temp. and humidity.
// Rso means resistance of the sensor in 150ppm CO gas under 20°C/55%RH

const float gm702b_rh_offset[4][7] PROGMEM = {
  { -10.0, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0 }, // °C
  { 1.71, 1.58, 1.45, 1.38, 1.13, 1.01, 0.88 }, // Rs/R0 @ 30%RH
  { 1.47, 1.32, 1.28, 1.08, 0.98, 0.88, 0.72 }, // Rs/R0 @ 60%RH
  { 1.28, 1.15, 1.08, 0.90, 0.87, 0.71, 0.68 }  // Rs/R0 @ 85%RH
};

const float gm702b_u2gas[2][9] PROGMEM = {
  { 0.25, 0.65, 0.98, 1.35, 1.8, 1.98, 2.1, 2.38, 2.42 }, // V
  { 0.0, 5.0, 10.0, 20.0, 50.0, 100.0, 160.0, 500.0, 1000.0 }  // CO [ppm]
};

bool setupMultiGas(int16_t y) {
  // If you have changed the I2C address of gas sensor, you must to be specify the address of I2C.
  //The default addrss is 0x08;
  multigas.begin(Wire, 0x08); // use the hardware I2C
  //gas.begin(MyWire, 0x08); // use the software I2C
  //gas.setAddress(0x64); change thee I2C address
  return true;
}

float u_corr_rh(float u, float temp, float humidity, float* u_corr, size_t size) {
  size_t hum_idx1;
  size_t hum_idx2;
  float ref_hum1, ref_hum2;
  if (humidity <= 30.0) {
    hum_idx1 = 1;
    hum_idx2 = 1;
    ref_hum1 = 30.0;
    ref_hum2 = 60.0;
  } else if (humidity <= 60.0) {
    hum_idx1 = 1;
    hum_idx2 = 2;
    ref_hum1 = 30.0;
    ref_hum2 = 60.0;
  } else if (humidity <= 85.0) {
    hum_idx1 = 2;
    hum_idx2 = 3;
    ref_hum1 = 60.0;
    ref_hum2 = 85.0;
  } else {
    hum_idx1 = 3;
    hum_idx2 = 3;
    ref_hum1 = 60.0;
    ref_hum2 = 85.0;
  }
  size_t hum_off1 = size * hum_idx1;
  size_t hum_off2 = size * hum_idx2;
  // First get Rs/R0
  float old_rsr01 = *(u_corr + hum_off1);
  float old_rsr02 = *(u_corr + hum_off2);
  float rsr01 = old_rsr01;
  float rsr02 = old_rsr02;
  float old_temp = u_corr[0];
  if (temp >= old_temp) {
    for (size_t i = 1; i < size; i++) {
      float new_temp = *(u_corr + i);
      rsr01 = *(u_corr + hum_off1 + i);
      rsr02 = *(u_corr + hum_off2 + i);
      if (temp <= new_temp) {
        old_rsr01 += (temp - old_temp) / (new_temp - old_temp) * (rsr01 - old_rsr01);
        old_rsr02 += (temp - old_temp) / (new_temp - old_temp) * (rsr02 - old_rsr02);
        break;
      }
      old_temp = new_temp;
      old_rsr01 = rsr01;
      old_rsr02 = rsr02;
    }
  }
  float fact = (old_rsr01 + (humidity - ref_hum1) / (ref_hum2 - ref_hum1) * (old_rsr02 - old_rsr01));
  return u / fact;
}

float u2ppm(float u, float* u2gas, size_t size) {
  float old_ppm = *(u2gas + size);
  float old_u = u2gas[0];
  if (u <= old_u) {
    return old_ppm;
  }
  for (size_t i = 1; i < size; i++) {
    float new_u = *(u2gas + i);
    float ppm = *(u2gas + size + i);
    if (u <= new_u) {
      return old_ppm + (u - old_u) / (new_u - old_u) * (ppm - old_ppm);
    }
    old_u = new_u;
    old_ppm = ppm;
  }
  return old_ppm;
}

float getNO2ppm(uint32_t raw, float temp, float humidity) {
  float no2_u = multigas.calcVol(raw);
  float no2_corr = u_corr_rh(no2_u, temp, humidity, (float *)gm102b_rh_offset, 7);
  return u2ppm(no2_corr, (float *)gm102b_u2gas, 12);
}

float getC2H5OHppm(uint32_t raw, float temp, float humidity) {
  float c2h5oh_u = multigas.calcVol(raw);
  float c2h5oh_corr = u_corr_rh(c2h5oh_u, temp, humidity, (float *)gm302b_rh_offset, 13);
  return u2ppm(c2h5oh_corr, (float *)gm302b_u2gas, 11);
}

float getVOCppm(uint32_t raw, float temp, float humidity) {
  float voc_u = multigas.calcVol(raw);
  float voc_corr = u_corr_rh(voc_u, temp, humidity, (float *)gm502b_rh_offset, 13);
  return u2ppm(voc_corr, (float *)gm502b_u2gas, 9);
}

float getCOppm(uint32_t raw, float temp, float humidity) {
  float co_u = multigas.calcVol(raw);
  float co_corr = u_corr_rh(co_u, temp, humidity, (float *)gm702b_rh_offset, 7);
  return u2ppm(co_corr, (float *)gm702b_u2gas, 9);
}

void updateMultiGas(multigas_t *p) {
  p->no2 = multigas.measure_NO2();
  p->c2h5oh = multigas.measure_C2H5OH();
  p->voc = multigas.measure_VOC();
  p->co = multigas.measure_CO();
}

#endif // _MULTIGAS_H
