#ifndef _SDS011_H
#define _SDS011_H

#include <SdsDustSensor.h>

#include "common.h"

// Pins
#define SDS011_RX_PIN 5
#define SDS011_TX_PIN 4

// Interface is UART

SdsDustSensor sds(Serial1);

void readSDSInfo(bool serial = false) {
  FirmwareVersionResult firmwareVersion = sds.queryFirmwareVersion();
  ReportingModeResult reportingMode = sds.setActiveReportingMode();
  WorkingPeriodResult workingPeriod = sds.setContinuousWorkingPeriod();
  sdsInfo.sds_info_id = 0x53445330; // SDS0
  sdsInfo.date.year = firmwareVersion.year;
  sdsInfo.date.month = firmwareVersion.month;
  sdsInfo.date.day = firmwareVersion.day;
  sdsInfo.active = reportingMode.isActive();
  sdsInfo.period = workingPeriod.period;
  if (serial) {
    Serial.println("I:FW=" + firmwareVersion.toString()); // prints firmware version
    Serial.println("I:RM=" + reportingMode.toString()); // ensures sensor is in 'active' reporting mode
    Serial.println("I:WP=" + workingPeriod.toString()); // ensures sensor has continuous working period - default but not recommended
  }
}

bool setupSDS011(int16_t y) {
  Serial1.begin(9600, SERIAL_8N1, SDS011_RX_PIN, SDS011_TX_PIN);
  readSDSInfo(y > 0);
  bool valid = sds.readPm().isOk();
  if (y > 0) {
    sprintf(buffer, "I:SDS Valid=%d", valid);
    Serial.println(buffer);
  }
  sdsData.valid = false;
  sdsData.pm25 = 0;
  sdsData.pm10 = 0;
  return valid;
}

void updateSDS011() {
  uint32_t available = Serial2.available();
  if (available < 256) {
    sdsInfo.available = available;
  } else {
    sdsInfo.available = 255;
  }
  PmResult pm = sds.readPm();
  // Clear read buffer
  while (Serial1.available()) {
    Serial1.read();
  }
  if (pm.isOk()) {
    if (sdsInfo.date.year == 65535) {
      readSDSInfo();
    }
    sdsData.valid = true;
    sdsData.pm25 = lround(pm.pm25 * 10.0);
    sdsData.pm10 = lround(pm.pm10 * 10.0);
  } else {
    sdsData.valid = false;
    SDS011Error = pm.statusToString();
    if (SerialOut) {
      Serial.print("E:Could not read values from sensor, reason: ");
      Serial.println(SDS011Error);
    }
    setupSDS011(0);
  }
}

void sds011Task(void *arg) {
  for (;;) {
    updateSDS011();
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

#endif // _SDS011_H
