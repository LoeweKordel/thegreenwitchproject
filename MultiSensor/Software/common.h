#ifndef _COMMON_H
#define _COMMON_H

#include <sd_defines.h>
#include <sd_diskio.h>
#include <SD.h>
#include <AsyncJson.h>

#define HW_VERSION      3  // Hardware version
#define SW_VERSION      5  // Software version
#define DATA_VERSION    7
#define SERIAL_NR       1  // Serial number

#define DATA_SIZE      36  // After every 60 measurements (3 min) write them to a file.
#define PRESSURE_SIZE 100  // Size of magnet buffer (fast ~20 samples/s)
#define TLV493D_SIZE  100  // Size of magnet buffer (fast ~20 samples/s)
#define ICM20600_SIZE  10  // Size of accel and gyro buffer (~2 samples/s)
#define AK09918_SIZE  100  // Size of magnet buffer (fast ~20 samples/s)
#define HISTORY_SIZE  (unsigned int)180  // 15min (each 5s one sample)

#define BUFFER_SIZE 4096

char buffer[BUFFER_SIZE];
char tmpbuffer[BUFFER_SIZE];

enum DISP {
  SENSORS,
  POSITION,
  TEMPERATURE,
  HUMIDITY,
  PRESSURE,
  ALTITUDE,
  CO2,
  O2,
  NO2,
  C2H5OH,
  VOC,
  CO,
  PM25,
  PM10,
  DEV_INFO,
  CNT
};

#define BACKLIGHT_OFF_TIME 250

// Define Limits (Low Error, Low Warning, High Warning, High Error)
const float co2_limits[] PROGMEM = { 0.0, 0.0, 1000.0, 2000.0 }; // pppm
const float o2_limits[] PROGMEM = { 18.0, 19.0, 24.0, 25.0 }; // %
const float no2_limits[] PROGMEM = { 0.0, 0.0, 30.0, 40.0 }; // ug/m3
const float c2h5oh_limits[] PROGMEM = { 0.0, 0.0, 30.0, 40.0 }; // ug/m3
const float voc_limits[] PROGMEM = { 0.0, 0.0, 30.0, 40.0 }; // ug/m3
const float co_limits[] PROGMEM = { 0.0, 0.0, 20.0, 68.0 }; // ppm (20ppm=25mg/m3, 68ppm=85mg/m3)
const float pm25_limits[] PROGMEM = { 0.0, 0.0, 15.0, 20.0 }; // ug/m3
const float pm10_limits[] PROGMEM = { 0.0, 0.0, 30.0, 40.0 }; // ug/m3

struct avg_min_max_t {
  float avg;
  float min;
  float max;
};

struct xyz_t {
  uint16_t x; // float16
  uint16_t y; // float16
  uint16_t z; // float16
} __attribute__((packed));

struct ms_date_t {
  uint16_t year;
  uint8_t month;
  uint8_t day;
} __attribute__((packed));

struct ms_time_t {
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
} __attribute__((packed));

struct bmp180_t {
  uint8_t cnt;                      // Number of samples in buffer
  int16_t temp;                     // °C (2 decimals)
  uint16_t pressure[PRESSURE_SIZE]; // hPa (1 decimal)
} __attribute__((packed));

struct bme280_t {
  uint8_t cnt;                      // Number of samples in buffer
  int16_t temp;                     // °C (2 decimals)
  int16_t humidity;                 // % (1 decimal)
  uint16_t pressure[PRESSURE_SIZE]; // hPa (1 decimal)
} __attribute__((packed));

struct dht20_t {
  int16_t temp;     // °C (2 decimals)
  int16_t humidity; // % (1 decimal)
} __attribute__((packed));

struct tlv493d_t {
  uint8_t error;
  uint8_t cnt;                // Number of samples in buffer
  xyz_t magnet[TLV493D_SIZE]; // mT (TLV493D)
  int16_t temp;               // °C (2 decimals)
} __attribute__((packed));

struct imu9dof_t {
  uint8_t icm20600_cnt;
  uint8_t ak09918_cnt;
  uint8_t ak09918_error;
  xyz_t accel[ICM20600_SIZE]; // m/s^2
  xyz_t gyro[ICM20600_SIZE];  // DEG/s
  xyz_t magnet[AK09918_SIZE]; // uT
  int16_t temp;               // °C (2 decimals)
} __attribute__((packed));

struct sds011_t {
  bool valid;
  int16_t pm25; // [ug/m3] PM2.5 (1 decimals)
  int16_t pm10; // [ug/m3] PM10 (1 decimals)
} __attribute__((packed));

struct mhz19b_t {
  int16_t co2;  // CO2 in ppm (0 decimals)
  int16_t temp; // °C (2 decimals)
  uint8_t error;
} __attribute__((packed));

struct mix8410_t {
  float u; // V
} __attribute__((packed));

struct multigas_t {
  uint32_t no2;    // Raw value
  uint32_t c2h5oh; // Raw value
  uint32_t voc;    // Raw value
  uint32_t co;     // Raw value
} __attribute__((packed));

struct gps_t {
  ms_date_t date;
  ms_time_t time;
  uint8_t satellites;
  float latitude;
  float longitude;
  uint16_t altitude;     // m (1 decimals)
  uint16_t speed;        // km/h (1 decimals)
  float angle;
  float hdop;
  float vdop;
  float pdop;
  bool valid;            // Have a fix?
  uint8_t quality;       // Fix quality (0, 1, 2 = Invalid, GPS, DGPS)
  uint8_t quality_3d;    // 3D fix quality (1, 3, 3 = Nofix, 2D fix, 3D fix)
} __attribute__((packed));

struct data_t {
  uint32_t counter;     // 4 byte
  ms_date_t date;       // 4 byte
  ms_time_t time;       // 3 byte
  bmp180_t bmp180;      // 203 (min 5) byte
  bme280_t bme280;      // 205 (min 7) byte
  dht20_t dht20;        // 4 byte
  tlv493d_t tlv493d;    // 604 (min 10) byte
  imu9dof_t imu9dof;    // 725 (min 23) byte
  sds011_t sds011;      // 5 byte
  mhz19b_t mhz19;       // 5 byte
  mix8410_t mix8410;    // 4 byte
  multigas_t multigas;  // 16 byte
  gps_t pa1010d;        // 39 byte
} __attribute__((packed)); // 1821 (min 129) byte

struct temp_t {
  float bmp180;
  float bme280;
  float dht20;
  float tlv493d;
  float icm20600;
  float mhz19; // Ignored in calculation. Seems to be sensor inner temperature.
};

struct humidity_t {
  float bme280;
  float dht20;
};

struct pressure_t {
  float bmp180;
  float bme280;
};

struct offsets_t {
  float bmp180_temp;
  float bmp180_pressure;
  float bme280_temp;
  float bme280_pressure;
  float bme280_humidity;
  float dht20_temp;
  float dht20_humidity;
  float mhz19_temp;
  float mhz19_co2;
  float mix8410_u;
  float multigas_no2;
  float multigas_c2h5oh;
  float multigas_voc;
  float multigas_co;
  float tlv493d_temp;
  float imu9dof_axe;
  float imu9dof_aye;
  float imu9dof_aze;
  float imu9dof_gxe;
  float imu9dof_gye;
  float imu9dof_gze;
  float imu9dof_temp;
} __attribute__((packed)); // 88 byte

struct sds_info_t {
  bool valid;
  uint32_t sds_info_id; // SDS0
  ms_date_t date; // Firmware version
  bool active; // Active mode?
  uint8_t period; // Working period
  uint8_t available; // Available bytes on serial port
} __attribute__((packed)); // 12 byte

struct file_header_t {
  uint16_t id; // Identifier
  uint32_t serial_nr;
  uint8_t hw_version;
  uint8_t sw_version;
  uint8_t data_version;
  char mhz19_version[4];
  uint8_t data_cnt; // Number of data sets
  uint32_t startup_cnt;
  uint32_t crc; // CRC of data sets
} __attribute__((packed)); // 22 byte

struct sd_info_t {
  sdcard_type_t card_type;
  uint64_t card_size;
  uint64_t total_bytes;
  uint64_t used_bytes;
  uint32_t file_cnt;
} __attribute__((packed)); // 15 byte

struct clock_info_t {
  bool valid;
  uint8_t error;
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
} __attribute__((packed));

struct display_info_t {
  uint8_t data_cnt;
  float temp;
  float temp_min;
  float temp_max;
  float humidity;
  float humidity_min;
  float humidity_max;
  float pressure;
  float pressure_min;
  float pressure_max;
  uint16_t altitude;
  float atm;
  float co2; // [ppm] from MHZ19
  float co2_max; // [ppm] from MHZ19
  float o2; // [ppm] from MIX8410
  float o2_max; // [ppm] from MIX8410
  float no2; // [ppm] from MultiGas
  float no2_max; // [ppm] from MultiGas
  float c2h5oh; // [ppm] from MultiGas
  float c2h5oh_max; // [ppm] from MultiGas
  float voc; // [ppm] from MultiGas
  float voc_max; // [ppm] from MultiGas
  float co; // [ppm] from MultiGas
  float co_max; // [ppm] from MultiGas
  bool pm_valid;
  float pm25; // [ug/m3]
  float pm25_max; // [ug/m3]
  float pm10; // [ug/m3]
  float pm10_max; // [ug/m3]
  uint8_t pa1010d_sat;
  float pa1010d_alt;
  float pa1010d_lng;
  float pa1010d_lat;
} __attribute__((packed));

sds_info_t sdsInfo;
sds011_t sdsData;
sd_info_t sdInfo;
clock_info_t clockInfo;
display_info_t displayInfo;

uint8_t SerialOut = 1;
bool BacklightEnabled = true;
uint8_t backlight_off = BACKLIGHT_OFF_TIME;
bool WiFiEnabled = false;
bool WiFiScanning = false;
int8_t WiFiRSSI = 0;
uint8_t GpsAvailable = 0;
bool RecordingEnabled = true;
bool FtpConnected = false;
sdcard_type_t oldSdCardType = CARD_NONE;
bool DebugMode = true;
enum DISP DisplayPage = SENSORS;
bool DisplayAlwaysOn = false;
uint8_t MenuPos = 255; // 255 = Menu not shown
bool UpdateMenu = false;
bool ShowTiming = false;

unsigned long start_time = millis() / 1000;
unsigned long operating_time = 0; // Operating time in minutes

uint32_t startup_cnt = 0;
uint32_t counter = 0;
int data_cnt = 0;
data_t *data = NULL;
data_t *curData = NULL;
offsets_t offsets;
temp_t temp;
humidity_t humidity;
pressure_t pressure;

int16_t histTemp[HISTORY_SIZE];  // (int/10) [°C] Temperature history
int16_t histHumidity[HISTORY_SIZE];  // (int/10) [%] Temperature history
int16_t histPressure[HISTORY_SIZE];  // (int) [hPa] Temperature history
int16_t histAltitude[HISTORY_SIZE];  // (int) [m] Temperature history
int16_t histCO2[HISTORY_SIZE];  // (int/10) [ppm] CO2 history
int16_t histO2[HISTORY_SIZE];  // (int/10) [%] CO2 history
int16_t histNO2[HISTORY_SIZE];  // (int/10) [ppm] CO2 history
int16_t histC2H5OH[HISTORY_SIZE];  // (int/10) [ppm] CO2 history
int16_t histVOC[HISTORY_SIZE];  // (int/10) [ppm] CO2 history
int16_t histCO[HISTORY_SIZE];  // (int/10) [ppm] CO2 history
int16_t histPM25[HISTORY_SIZE];  // (int/10) [ug/m3] Temperature history
int16_t histPM10[HISTORY_SIZE];  // (int/10) [ug/m3] Temperature history

String SDS011Error = "";

String pa1010dData = "";

char fileName[] = "/B0000000.bin";

float fMin(float v1, float v2) {
  if (v1 < v2) {
    return v1;
  }
  return v2;
}

float calcAltitude(const float pressure, const float seaLevel = 1013.25) {
  /*!
  @brief     This converts a pressure measurement into a height in meters
  @details   The corrected sea-level pressure can be passed into the function if it is known,
             otherwise the standard atmospheric pressure of 1013.25hPa is used (see
             https://en.wikipedia.org/wiki/Atmospheric_pressure) for details.
  @param[in] pressure Pressure reading from BMP180/BME280
  @param[in] seaLevel Sea-Level pressure in millibars
  @return    floating point altitude in meters.
  */
  return 44330.0 * (1.0 - pow(pressure / seaLevel, 0.1903));  // Convert into meters
}

void calcAvgMinMax(float *array, uint8_t cnt, avg_min_max_t *dst) {
  float avg = 0.0;
  float min = array[0];
  float max = array[0];

  for (uint8_t i = 0; i < cnt; i++) {
    float value = array[i];

    avg += value;
    if (value < min) min = value;
    if (value > max) max = value;
  }
  dst->avg = avg / (float)cnt;
  dst->min = min;
  dst->max = max;
}

float getAvgTemperature() {
  float value = 0.0;
  uint8_t cnt = 0;

  if (!isnan(temp.bmp180)) {
    value += temp.bmp180;
    cnt++;
  }
  if (!isnan(temp.dht20)) {
    value += temp.dht20;
    cnt++;
  }
  /*if (!isnan(temp.bme280)) {
    value += temp.bme280;
    cnt++;
  }
  if (!isnan(temp.tlv493d)) {
    value += temp.tlv493d;
    cnt++;
  }
  if (!isnan(temp.icm20600)) {
    value += temp.icm20600;
    cnt++;
  }*/
  if (cnt == 0) {
    return 0.0;
  }
  return value / (float)cnt;
}

float getMinTemperature() {
  float value = 99.9;

  if (!isnan(temp.bmp180)) {
    value = temp.bmp180;
  }
  if (!isnan(temp.dht20)) {
    value = min(value, temp.dht20);
  }
  /*if (!isnan(temp.bme280)) {
    value = min(value, temp.bme280);
  }
  if (!isnan(temp.tlv493d)) {
    value = min(value, temp.tlv493d);
  }
  if (!isnan(temp.icm20600)) {
    value = min(value, temp.icm20600);
  }*/
  return value;
}

float getMaxTemperature() {
  float value = -99.9;

  if (!isnan(temp.bmp180)) {
    value = temp.bmp180;
  }
  if (!isnan(temp.dht20)) {
    value = max(value, temp.dht20);
  }
  /*if (!isnan(temp.bme280)) {
    value = max(value, temp.bme280);
  }
  if (!isnan(temp.tlv493d)) {
    value = max(value, temp.tlv493d);
  }
  if (!isnan(temp.icm20600)) {
    value = max(value, temp.icm20600);
  }*/
  return value;
}

float getAvgHumidity() {
  return (humidity.bme280 + humidity.dht20) * 0.5;
}

float getMinHumidity() {
  return min(humidity.bme280, humidity.dht20);
}

float getMaxHumidity() {
  return max(humidity.bme280, humidity.dht20);
}

float getAvgPressure() {
  return (pressure.bmp180 + pressure.bme280) * 0.5;
}

float getMinPressure() {
  return min(pressure.bmp180, pressure.bme280);
}

float getMaxPressure() {
  return max(pressure.bmp180, pressure.bme280);
}

void updateHistory(int16_t *history, float value, float mul)
{
  for (int i = HISTORY_SIZE - 1; i > 0; i--) {
    history[i] = history[i - 1];
  }
  history[0] = (int16_t)lround(value * mul);
}

void setBacklight(bool enabled);
void clearScreen();
void showMessage(char* msg);
void updateStatusBar(bool force);
void updateDisplay(bool force);
void updateUser(bool force);
void showMenu(bool selected);
bool sendSdCardContentsChunk(uint32_t nr, uint32_t cnt, String &fileNames, AsyncWebSocket &ws);
void hideSDIcon();
void hideWiFiIcon();
void clearMax();

#endif // _COMMON_H
