import asyncio
import os
import traceback
from datetime import datetime
from typing import List, Dict, TYPE_CHECKING, Tuple

from matplotlib.backends.backend_qtagg import FigureCanvas
from matplotlib.figure import Figure
import matplotlib.dates as mdates

from scandir_rs import Scandir

from PySide6.QtGui import QIcon
from PySide6.QtCore import Qt, QSettings
from PySide6.QtWidgets import QTreeWidgetItem, QTreeWidget, QMessageBox, QLabel, QFrame

from multisensor import BinFile, load_bin_file, merge_bin_files
from util.eventloop import QEventLoopStackTraced
from util.db import LoadBinFileFailed

if TYPE_CHECKING:
    from greenwitch import MainWindow


def ErrorDialog(text: str):
    print(text)
    msgBox = QMessageBox(QMessageBox.Critical, "The Green Witch - Error", text)
    msgBox.setWindowIcon(QIcon(":images/app-error.png"))
    msgBox.exec()


def QuestionDialog(text: str):
    msgBox = QMessageBox(QMessageBox.Question, "The Green Witch - Question", text,
                         buttons=QMessageBox.StandardButtons.Yes | QMessageBox.StandardButtons.No)
    msgBox.setWindowIcon(QIcon(":images/app-question.png"))
    return msgBox.exec() == QMessageBox.Yes


def SetColumnWidth(tw: QTreeWidget, settings: QSettings, key: str):
    for i in range(tw.columnCount()):
        w = int(settings.value(f"{key}Col{i}", tw.columnWidth(i)))
        print(i, w)
        tw.setColumnWidth(i, w)


def GetColumnWidth(tw: QTreeWidget, settings: QSettings, key: str):
    for i in range(tw.columnCount()):
        settings.setValue(f"{key}Col{i}", tw.columnWidth(i))


def RemoveFiles(tw: QTreeWidget, dirName: str) -> bool:
    rows = {index.row() for index in tw.selectionModel().selectedIndexes()}
    if not QuestionDialog(f"Do you really want to delete {len(rows)} files?"):
        return False
    fileDirNames = set()
    for row in reversed(sorted(rows)):
        item = tw.takeTopLevelItem(row)
        if dirName == "data":
            fileDirName = f"{dirName}/{item.text(1)}"
            fileDirNames.add(fileDirName)
            os.remove(f"{fileDirName}/{item.text(0)}")
        else:
            os.remove(f"{dirName}/{item.text(0)}")
    # Remove empty dirs
    for fileDirName in fileDirNames:
        if not os.listdir(fileDirName):
            os.rmdir(fileDirName)
    return True


def GetRecordInfo(file: BinFile) -> str:
    return f"Record {file.cnt:4} data points. Start @ {file.start}. Duration is {file.duration}. GPS: {file.has_gps_str()}"


def ScanDataDir(loop: QEventLoopStackTraced, app: 'MainWindow') -> Tuple[dict, dict]:
    binFiles: Dict[str, List[BinFile]] = {}
    errors: Dict[str, List[Tuple[str, int, str]]] = {}
    for entry in sorted(Scandir("data"), key=lambda x: x.path):
        if not entry.is_file or not entry.path.endswith(".bin"):
            continue
        pathName = f"data/{entry.path}"
        fileName = os.path.basename(pathName)
        dirName = os.path.basename(os.path.dirname(pathName))
        try:
            file: BinFile = load_bin_file(pathName)
            binFiles.setdefault(dirName, []).append(file)
            item = QTreeWidgetItem([fileName, dirName, file.filename, file.start_date_time_str, file.duration_str,
                                    str(file.version)])
        except Exception as exc:
            errMsg = traceback.format_exc()
            excMsg = str(exc)
            if excMsg == "File incomplete":
                msg = excMsg
            elif "Unsupported data_version" in excMsg:
                msg = excMsg.split("Unsupported ")[1]
            else:
                msg = ""
            item = QTreeWidgetItem([fileName, dirName, "FAILED", msg, "", "", ""])
            for cn in range(3):
                item.setToolTip(cn, errMsg)
            errors.setdefault(dirName, []).append((pathName, os.path.getsize(pathName), errMsg))
        loop.call_soon_threadsafe(app.twDataFiles.addTopLevelItem, item)
    loop.call_soon_threadsafe(app.twDataFiles.sortItems, 0, Qt.SortOrder.AscendingOrder)
    return binFiles, errors


def MergeBinFiles(binFiles: Dict[str, List[BinFile]], bRemoveBinFiles: bool) -> Tuple[dict, dict]:
    filePaths = [file.path for dirFiles in binFiles.values() for file in dirFiles]
    try:
        return merge_bin_files(filePaths, out_dir="records", remove_bin_files=bRemoveBinFiles)
    except Exception as exc:
        return {}, {"*": str(exc)}


def DeletEmptyDirectories():
    # Delete empty directories
    for dirName in os.listdir("data"):
        pathName = f"data/{dirName}"
        if not os.listdir(pathName):
            os.rmdir(pathName)


def UpdateRecordFiles(loop: QEventLoopStackTraced, app: 'MainWindow') -> Dict[str, str]:
    DeletEmptyDirectories()
    app.db.update()
    items = []
    errors = {}
    for key, entry in sorted(app.db.db().items()):
        if isinstance(entry, LoadBinFileFailed):
            item = QTreeWidgetItem([os.path.basename(entry.path), "FAILED", "", "", key])
            item.setToolTip(0, entry.message)
            errors[key] = entry.message
        else:
            item = QTreeWidgetItem([entry["filename"], entry["start_date_time_str"], entry["duration_str"], entry["has_gps_str"], key])
        items.append(item)
    if items:
        loop.call_soon_threadsafe(app.twRecords.addTopLevelItems, items)
    loop.call_soon_threadsafe(app.twRecords.sortItems, 0, Qt.SortOrder.AscendingOrder)
    return errors


def AddTitleLabel(text: str) -> QLabel:
    lbl = QLabel(text)
    lbl.setFrameShape(QFrame.Shape.WinPanel)
    lbl.setFrameShadow(QFrame.Shadow.Raised)
    return lbl


def CreatePlot(unit: str, x: List[datetime], y: List[float] | Dict[str, List[float]]):
    fig = FigureCanvas(Figure(figsize=(5, 3)))
    sub = fig.figure.subplots()
    sub.xaxis.set_major_formatter(mdates.DateFormatter('%Y.%m.%d %H:%M'))
    sub.yaxis.set_label(f"[{unit}]")
    if isinstance(y, list):
        sub.plot(x, y)
    else:
        for k, v in sorted(y.items()):
            sub.plot(x, v, label=k)
        sub.legend()
    for tick in sub.get_xticklabels():
        tick.set_rotation(45)
    fig.figure.subplots_adjust(left=0.08, right=0.99, top=0.98, bottom=0.21)
    return fig, sub
