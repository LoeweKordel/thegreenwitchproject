import json
from typing import List

import websockets


async def GetFileList(ip: str) -> List[str]:
    print("Get SD-Card contents...")
    async with websockets.connect(f"ws://{ip}/ws") as ws:
        await ws.recv()
        offset = 0
        allFileNames = []
        while True:
            print(f"--> list_sdcard_files {offset}")
            await ws.send(f"list_sdcard_files {offset}")
            result = await ws.recv()
            if not result:
                print("==> FIN1")
                break
            data = json.loads(result)
            if data.get("filename_nr") == -1:
                print("==> FIN2")
                break
            fileNames = data.get("filenames")
            if fileNames:
                print(f"++> {len(fileNames)}")
                fileNames = [fileName[1:] for fileName in fileNames.splitlines() if fileName.endswith(".bin")]
                offset += len(fileNames)
                allFileNames.extend(fileNames)
                print(f"==> {offset} {len(fileNames)} files")
    print(f"GetFileList ALL {len(allFileNames)} files")
    return allFileNames
