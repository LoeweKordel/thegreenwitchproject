import asyncio
import os
from asyncio import Task
from datetime import date
from threading import Event
from typing import List, Dict, Tuple, Set
import traceback

from ftplib import FTP

from multisensor import BinFile, load_bin_file

from util.eventloop import QEventLoopStackTraced

from PySide6.QtWidgets import QLabel, QProgressBar, QApplication


def DeleteDataFiles(ip: str, fileNames: List[str], loop: QEventLoopStackTraced, lbl: QLabel, pb: QProgressBar,
                    twCb: callable, evtCancel: Event):
    print("DeleteDataFiles", len(fileNames), fileNames)
    with FTP(ip, timeout=10) as ftp:
        print("Login...")
        loop.call_soon_threadsafe(lbl.setText, "Login...")
        ftp.login("MS01", "MS01")
        cnt = len(fileNames)
        for nr, fileName in enumerate(reversed(fileNames)):
            if evtCancel.is_set():
                break
            print(f"Delete {nr + 1}/{cnt} {fileName}")
            loop.call_soon_threadsafe(lbl.setText, f"Delete {nr + 1}/{cnt} {fileName}")
            ftp.delete(fileName)
            loop.call_soon_threadsafe(twCb, cnt - nr - 1, fileName, "DELETED", "", 0, "")
            loop.call_soon_threadsafe(pb.setValue, nr + 1)
            pending: Set[Task] = asyncio.all_tasks()
            asyncio.wait(pending)
            QApplication.processEvents()
    print("DeleteDataFiles FIN")


def DownloadDataFiles(ip: str, fileNames: List[str], loop: QEventLoopStackTraced, lbl: QLabel, pb: QProgressBar,
                      twCb: callable, evtCancel: Event) -> Tuple[str, Dict[str, BinFile], Dict[str, str]]:
    files: Dict[str, BinFile] = {}
    errors: Dict[str, str] = {}
    with FTP(ip, timeout=10) as ftp:
        loop.call_soon_threadsafe(lbl.setText, "Login...")
        ftp.login("MS01", "MS01")
        loop.call_soon_threadsafe(lbl.setText, "Get SD-Card contents...")
        dataDirName = "data" if os.path.exists("data") else "../data"
        today = date.today()
        dirNameBase = today.strftime(f"{dataDirName}/%Y.%m.%d")
        dirName = dirNameBase
        # Only choose new directory if local files intersect with remote files
        if os.path.exists(dirNameBase) and set(os.listdir(dirNameBase)).intersection(fileNames):
            nr = 1
            while os.path.exists(dirName):
                nr += 1
                dirName = f"{dirNameBase}-{nr}"
        loop.call_soon_threadsafe(lbl.setText, f"Download {len(fileNames)} data files to {dirName}...")
        if not os.path.exists(dirName):
            os.mkdir(dirName)
        for nr, fileName in enumerate(fileNames):
            if evtCancel.is_set():
                break
            loop.call_soon_threadsafe(lbl.setText, f"Download {nr + 1}/{len(fileNames)} {fileName}...")
            filePath: str = f"{dirName}/{fileName}"
            try:
                with open(filePath, 'wb') as F:
                    ftp.retrbinary(f"RETR {fileName}", F.write)
                file: BinFile = load_bin_file(filePath)
                files[fileName] = file
                loop.call_soon_threadsafe(twCb, nr, file.filename, file.start_date_time_str, file.duration_str,
                                          file.version, file.has_gps_str())
            except TimeoutError as exc:
                loop.call_soon_threadsafe(twCb, nr, "FAILED", str(exc), "", "", "", traceback.format_exc())
                loop.call_soon_threadsafe(lbl.setText, f"Timeout! Failed to receive file! Remove file {fileName} and abort.")
                os.remove(filePath)
                break
            except Exception as exc:
                loop.call_soon_threadsafe(twCb, nr, "FAILED", str(exc), "", "", "", traceback.format_exc())
                errors[fileName] = str(exc)
            loop.call_soon_threadsafe(pb.setValue, nr + 1)
        if not os.listdir(dirName):
            os.rmdir(dirName)
    return dirName, files, errors
