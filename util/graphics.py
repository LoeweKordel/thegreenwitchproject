
import time
import io
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor
from typing import TYPE_CHECKING, List, Dict

import branca
import folium
import matplotlib.dates as mdates
from PySide6.QtCore import Qt, QSize, Signal
from PySide6.QtGui import QIcon, QCloseEvent
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QListWidget, QSizePolicy, QAbstractItemView,\
    QLabel, QFrame, QSlider, QPushButton, QSpacerItem
from matplotlib.backends.backend_qtagg import FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from multisensor import BinFile, load_bin_file, merge_bin_files

from util.sensordata import getPressures, getNO2ppm, getC2H5OHppm, getVOCppm, getCOppm, getPm, getMagnet,\
    getGpsAltitude, LatLngAltType, getGPSPath, getGPSBoundingRect

if TYPE_CHECKING:
    from greenwitch import MainWindow

UNITS = {"Temperature": "°C", "Humidity": "%", "Pressure": "hPa",
         "CO2": "ppm", "O2": "%", "C2H5OH": "ppm",
         "NO2": "ppm", "VOC": "ppm", "CO": "ppm",
         "PM2.5": "ug/m^3", "PM10": "ug/m^3",
         "IMU9DOF": "uT", "TLV493D": "mT",
         "Altitude": "m"}


def ComputeGasMass(gasName: str, gasPPM: float, temp, pressure):
    # 1 atm = 1.01325 bar
    # d = (P * M) / (R * T)
    # Molar Mass CO2: 44.01 [kg/kmol]
    # Gas Constant CO2: R = 0.1189 [kJ/kg*K]
    # Volume CO2: 0.0943 [m^3/kmol]
    # 0°C = 273.15K
    # molarMass: float, gasConstant: float
    # Result is g/m^3
    if gasName == "CO2":
        M = 44.01
        R = 0.1189
    elif gasName == "NO2":
        M = 46.01
        R = 0.1807
    elif gasName == "O2":
        M = 31.99
        R = 0.2598
    elif gasName == "CO":
        M = 28.011
        R = 0.2968
    else:
        raise Exception(f"Gas {gasName} is not supported")
    gasRel = 1000 * gasPPM / 1000000  # 1000: kg -> g
    return [gasRel * (P * M) / (R * T) for P, T in zip(temp, pressure)]


def CreateTitleLabel(text: str) -> QLabel:
    lbl = QLabel(text)
    lbl.setFrameShape(QFrame.Shape.WinPanel)
    lbl.setFrameShadow(QFrame.Shadow.Raised)
    return lbl


def CreatePlot(unit: str, x: List[datetime], y: List[float] | Dict[str, List[float]]):
    fig = FigureCanvas(Figure(figsize=(5, 3)))
    sub = fig.figure.subplots()
    sub.xaxis.set_major_formatter(mdates.DateFormatter('%Y.%m.%d %H:%M'))
    sub.yaxis.set_label(f"[{unit}]")
    if isinstance(y, list):
        sub.plot(x, y)
    else:
        for k, v in sorted(y.items()):
            sub.plot(x, v, label=k)
        sub.legend()
    for tick in sub.get_xticklabels():
        tick.set_rotation(45)
    fig.figure.subplots_adjust(left=0.08, right=0.99, top=0.98, bottom=0.21)
    return fig, sub


class ChartWindow(QWidget):
    sigClose = Signal(object)

    def __init__(self, dateTimes: List[datetime], yValues: dict):
        super().__init__()
        self.dateTimes: List[datetime] = dateTimes
        self.yValues: dict = yValues
        self.titles = {}
        self.analyzeFigures = {}
        self.lines = {}
        vLayout = QVBoxLayout()
        for signal, y in yValues.items():
            if not y:
                continue
            unit = UNITS.get(signal)
            fig, sub = CreatePlot(unit, self.dateTimes, y)
            fig.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
            self.lines[signal] = sub.axvline(x=self.dateTimes[0], color="r")
            title = self.titles[signal] = CreateTitleLabel(f"{signal} [{unit}]")
            vLayout.addWidget(title)
            vLayout.addWidget(fig)
            toolbar = NavigationToolbar(fig, self)
            toolbar.setMinimumHeight(24)
            vLayout.addWidget(toolbar)
            self.analyzeFigures[signal] = (fig, sub)
        self.resize(640, 400)
        self.setLayout(vLayout)
        self.setWindowTitle(f"Signals {' '.join(sorted(self.analyzeFigures))}")
        self.show()

    def closeEvent(self, event: QCloseEvent) -> None:
        self.sigClose.emit(self)
        super().closeEvent(event)

    def updateView(self, startTime, endTime, curTime, pos):
        for signal, title in self.titles.items():
            title.setText(f"{title.text().split(']')[0]}]  time={curTime}  value={self.yValues[signal][pos]}")
        for line in self.lines.values():
            line.set_xdata(curTime)
        for fig, sub in self.analyzeFigures.values():
            sub.axes.set_xlim(startTime, endTime)
            fig.draw()


class MapWindow(QWidget):
    sigClose = Signal(object)

    def __init__(self, signal: str, gpsPath: Dict[datetime, LatLngAltType], colorRef: list | None):
        super().__init__()
        self.dateTimes: List[datetime] = sorted(gpsPath)
        self.gpsPath: Dict[datetime, LatLngAltType] = gpsPath
        self.colorRef: list | None = colorRef
        curTime = self.dateTimes[0]
        for t in self.dateTimes:
            if t > curTime:
                break
            curTime = t
        pos = self.gpsPath.get(curTime)
        lat, lng, dLat, dLng = getGPSBoundingRect(list(gpsPath.values()))
        self.analyzeMap = folium.Map(
            location=[lat + 0.5 * dLat, lng + 0.5 * dLng],
            tiles="Stamen Terrain",
            zoom_start=13
        )
        if colorRef:
            if None in colorRef:
                y0 = [d for d in colorRef if d is not None]
                if y0:
                    y0 = sum(y0) / len(y0)
                    colorRef = [d if d is not None else y0 for d in colorRef]
                else:
                    colorRef = None
            if colorRef:
                colormap = branca.colormap.linear.YlOrRd_09.scale(min(colorRef), max(colorRef)).to_step(10)
                route = [(pos.lat, pos.lng) for k, pos in sorted(self.gpsPath.items())]
                folium.ColorLine(positions=route, colormap=colormap, weight=10, colors=colorRef).add_to(self.analyzeMap)
                self.analyzeMap.add_child(colormap)
            self.gpsTrack = None
        else:
            gpsPos = [(pos.lat, pos.lng) for _, pos in sorted(gpsPath.items())]
            self.gpsTrack = folium.PolyLine(gpsPos).add_to(self.analyzeMap)
        self.posMarker = folium.Marker([pos.lat, pos.lng], popup="Current Position", tooltip=f"Satellites {pos.satellites}")
        self.posMarker.add_to(self.analyzeMap)
        data = io.BytesIO()
        self.analyzeMap.save(data, close_file=False)
        self.analyzeWeb = QWebEngineView()
        self.analyzeWeb.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        self.analyzeWeb.sizePolicy().setHorizontalStretch(3)
        self.analyzeWeb.setMinimumHeight(600)
        self.analyzeWeb.setHtml(data.getvalue().decode())
        self.resize(640, 400)
        vLayout = QVBoxLayout()
        vLayout.addWidget(self.analyzeWeb)
        self.setLayout(vLayout)
        self.setWindowTitle(f"Signal {signal}")
        self.show()

    def closeEvent(self, event: QCloseEvent) -> None:
        self.sigClose.emit(self)
        super().closeEvent(event)

    def updateView(self, startTime, endTime, curTime, _pos):
        curPos = self.gpsPath[curTime]
        fullGpsPath = sorted([(t, pos) for t, pos in self.gpsPath.items()])
        gpsPath = [(t, pos) for t, pos in fullGpsPath if startTime <= t <= endTime]
        cutLeft = [t for t, _ in fullGpsPath if t < startTime]
        cutRight = [t for t, _ in fullGpsPath if t > endTime]
        newMap = folium.Map()
        newMap.location = self.analyzeMap.location
        newMap.options = self.analyzeMap.options
        # noinspection PyProtectedMember
        for k, v in self.analyzeMap._children.items():
            if "poly_line" not in k and "color_line" not in k and "color_map" not in k and "marker" not in k:
                newMap.add_child(v)
        self.analyzeMap = newMap
        if self.colorRef:
            y = self.colorRef[len(cutLeft):len(self.colorRef) - len(cutRight)]
            if None in y:
                y0 = [d for d in y if d is not None]
                if y0:
                    y0 = sum(y0) / len(y0)
                    y = [d if d is not None else y0 for d in y]
                else:
                    y = None
            if y:
                colormap = branca.colormap.linear.YlOrRd_09.scale(min(y), max(y)).to_step(10)
                route = [(pos.lat, pos.lng) for k, pos in gpsPath]
                folium.ColorLine(positions=route, colormap=colormap, weight=10, colors=y).add_to(self.analyzeMap)
                self.analyzeMap.add_child(colormap)
        else:
            gpsPos = [(pos.lat, pos.lng) for _, pos in gpsPath]
            self.gpsTrack = folium.PolyLine(gpsPos).add_to(self.analyzeMap)
        if True or self.posMarker is None:
            self.posMarker = folium.Marker([curPos.lat, curPos.lng], popup="Current Position", tooltip=f"Satellites {curPos.satellites}")
            self.posMarker.add_to(self.analyzeMap)
        else:
            self.posMarker.location = [curPos.lat, curPos.lng]
            # noinspection PyProtectedMember
            for key, child in self.posMarker._children.items():
                if "tooltip" in key:
                    child.text = f"Satellites {curPos.satellites}"
        data = io.BytesIO()
        self.analyzeMap.save(data, close_file=False)
        self.analyzeWeb.setHtml(data.getvalue().decode())


class Charts:

    def __init__(self, app: 'MainWindow'):
        self.app: 'MainWindow' = app
        self.lwSensors = QListWidget()
        self.lwSensors.setSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)
        self.lwSensors.setMinimumWidth(100)
        self.lwSensors.addItems(["Temperature", "Humidity", "Pressure", "CO2", "O2", "NO2", "C2H5OH", "VOC", "CO",
                                 "PM2.5", "PM10", "Altitude"])  # , "IMU9DOF", "TLV493D"
        self.lwSensors.setSelectionMode(QAbstractItemView.SelectionMode.ExtendedSelection)
        self.lwSensors.itemSelectionChanged.connect(self.on_lwSensors_itemSelectionChanged)
        self.lwColorRef = QListWidget()
        self.lwColorRef.setSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)
        self.lwColorRef.setMinimumWidth(100)
        self.lwColorRef.addItems(["Temperature", "Humidity", "Pressure", "CO2", "O2", "NO2", "C2H5OH", "VOC", "CO",
                                  "PM2.5", "PM10", "Altitude"])  # , "IMU9DOF", "TLV493D"
        self.lwColorRef.itemSelectionChanged.connect(self.on_lwColorRef_itemSelectionChanged)
        self.btnAnalyzeShowCharts = None
        self.vLayout = QVBoxLayout()
        self.file: BinFile | None = None
        self.times = []
        self.curTime: datetime | None = None
        self.lblTime = None
        self.gpsPath = {}
        self.gpsTimeline = []
        self.trailCoordinates = []
        self.sliderStart: QSlider | None = None
        self.lblStartBegin: QLabel | None = None
        self.lblStartTime: QLabel | None = None
        self.lblStartEnd: QLabel | None = None
        self.sliderEnd: QSlider | None = None
        self.lblEndBegin: QLabel | None = None
        self.lblEndTime: QLabel | None = None
        self.lblEndEnd: QLabel | None = None
        self.sliderTime: QSlider | None = None
        self.lblTimeBegin: QLabel | None = None
        self.lblTimeTime: QLabel | None = None
        self.lblTimeEnd: QLabel | None = None
        self.analyzePosMarker = None
        self.analyzeFigures = {}
        self.yValues = {}
        self.gpsTrack = None
        self.colorRef: str | None = None
        self.line = None
        self.maps = []
        self.charts = []

    async def update(self, fileNames: List[str]):
        print("graphics.update", fileNames)
        app = self.app
        currentIndex = app.tbwDataView.currentIndex()
        # Cleanup
        for i in reversed(range(1, app.tbwDataView.count())):
            app.tbwDataView.removeTab(i)
        if not fileNames:
            return
        # Load file and update info tab
        with ThreadPoolExecutor() as executor:
            # noinspection PyTypeChecker
            futures = {fileName: executor.submit(load_bin_file, fileName) for fileName in fileNames}
            files = {fileName: future.result() for fileName, future in futures.items()}
        if len(files) == 1:
            file = self.file = tuple(files.values())[0]
            print("FILE comment", [file.comment])
        else:
            print("###", [file.path for file in files.values()])
            mergedFiles, errors = merge_bin_files([file.path for file in files.values()], force=True)
            print(mergedFiles)
            print("!!!", errors)
            for fileName, file in mergedFiles.items():
                print(f"{fileName} comment:", [file.comment])
            if not mergedFiles:
                return
            file = self.file = tuple(mergedFiles.values())[0]
        offsets = file.offsets
        text = "+".join(sorted(files))
        if len(text) > 40:
            text = text[:37] + "..."
        app.lblFileName.setText(text)
        app.lblStartTime.setText(file.start_date_time_str)
        app.lblEndTime.setText(file.end_date_time_str)
        app.lblDuration.setText(str(file.duration_str))
        app.lblSerialNumber.setText(str(file.file_header.serial_nr))
        app.lblHwVersion.setText(str(file.file_header.hw_version))
        app.lblSwVersion.setText(str(file.file_header.sw_version))
        app.lblDataVersion.setText(str(file.version))
        app.lblDataCount.setText(str(file.cnt))
        app.lblStartupCount.setText(str(file.file_header.startup_cnt))
        app.lblGPS.setText(file.has_gps_str())
        app.pteComment.setPlainText(file.comment)
        app.lblBMP180Offsets.setText(f"Temp={offsets.bmp180_temp} °C  Pressure={offsets.bmp180_pressure} hPa")
        app.lblBME280Offsets.setText(f"Temp={offsets.bme280_temp} °C  Pressure={offsets.bme280_pressure} hPa"
                                     f"  Humidity={offsets.bme280_humidity} %")
        app.lblDHT20Offsets.setText(f"Temp={offsets.dht20_temp} °C  Humidity={offsets.dht20_humidity} %")
        app.lblMHZ19Offsets.setText(f"Temp={offsets.mhz19_temp} °C  CO2={offsets.mhz19_co2} ppm")
        app.lblIMU9DOFOffsets.setText(f"Temp={offsets.imu9dof_temp} °C"
                                      f"  Accel={offsets.imu9dof_axe}, {offsets.imu9dof_aye}, {offsets.imu9dof_aze} m/s^2"
                                      f"  Gyro={offsets.imu9dof_gxe}, {offsets.imu9dof_gye}, {offsets.imu9dof_gze} rad/s")
        self.lwSensors.clearSelection()
        self.lwColorRef.clearSelection()
        # Add Tabs
        x = [data.date_time for data in file.data]
        self.addTab("Temperature", "°C", x, [data.temperature for data in file.data])
        self.addTab("Humidity", "%", x, [data.humidity for data in file.data])
        self.addTab("Pressure", "hPa", x, getPressures(file.data))
        self.addTab("CO2", "ppm", x, [data.co2 for data in file.data])
        self.addTab("O2", "%", x, [data.mix8410.u * 21.0 / 2.0 for data in file.data])
        self.addTab("NO2", "ppm", x, [getNO2ppm(data.multigas.no2, data.temperature, data.humidity) for data in file.data])
        self.addTab("C2H5OH", "ppm", x, [getC2H5OHppm(data.multigas.c2h5oh, data.temperature, data.humidity) for data in file.data])
        self.addTab("VOC", "ppm", x, [getVOCppm(data.multigas.voc, data.temperature, data.humidity) for data in file.data])
        self.addTab("CO", "ppm", x, [getCOppm(data.multigas.co, data.temperature, data.humidity) for data in file.data])
        self.addTab("PM", "ug/m^3", x,
                    {"PM2.5": getPm([data.pm25 for data in file.data]),
                     "PM10": getPm([data.pm10 for data in file.data])})
        accel = [data.imu9dof.accel for data in file.data]
        gyro = [data.imu9dof.gyro for data in file.data]
        self.addTab("IMU9DOF", "uT", *getMagnet([(data, data.imu9dof.magnet) for data in file.data]))
        self.addTab("TLV493D", "mT", *getMagnet([(data, data.tlv493d.magnet) for data in file.data]))
        if file.has_gps(True):
            # TODO: Show Altitude also calculated by pressure
            self.addTab("Altitude", "m", x, getGpsAltitude([data.pa1010d for data in file.data]))
            self.addGPSTab(file)
            self.addAnalyzeTab(file)
        app.tbwDataView.setCurrentIndex(currentIndex)

    def addTab(self, title: str, unit: str, xData: List[datetime], yData: List[float] | Dict[str, List[float]]):
        fig = CreatePlot(unit, xData, yData)[0]
        navBar = NavigationToolbar(fig)
        widget = QWidget()
        layout = QVBoxLayout(widget)
        layout.addWidget(fig)
        layout.addWidget(navBar)
        widget.setLayout(layout)
        self.app.tbwDataView.addTab(widget, title)

    def addGPSTab(self, file: BinFile):
        tbw = self.app.tbwDataView
        gpsPath: List[LatLngAltType] = getGPSPath([(data.date_time, data.pa1010d) for data in file.data])
        if not gpsPath:
            return
        lat, lng, dLat, dLng = getGPSBoundingRect(gpsPath)
        m = folium.Map(
            location=[lat, lng],
            tiles="Stamen Terrain",
            zoom_start=13
        )
        folium.PolyLine([(pos.lat, pos.lng) for pos in gpsPath], tooltip=file.filename).add_to(m)
        data = io.BytesIO()
        m.save(data, close_file=False)
        w = QWebEngineView()
        w.setHtml(data.getvalue().decode())
        widget = QWidget(tbw)
        layout = QVBoxLayout(widget)
        layout.addWidget(w)
        widget.setLayout(layout)
        tbw.addTab(widget, "GPS")

    def addAnalyzeTab(self, file: BinFile):

        def createToolbar():
            hLayout = QHBoxLayout()
            button = QPushButton(QIcon(":images/map.png"), "")
            button.setIconSize(QSize(48, 48))
            button.released.connect(self.on_btnAnalyzeShowMap)
            hLayout.addWidget(button)
            self.btnAnalyzeShowCharts = QPushButton(QIcon(":images/charts.png"), "")
            self.btnAnalyzeShowCharts.setIconSize(QSize(48, 48))
            self.btnAnalyzeShowCharts.released.connect(self.on_btnAnalyzeShowCharts)
            self.btnAnalyzeShowCharts.setEnabled(False)
            hLayout.addWidget(self.btnAnalyzeShowCharts)
            spacer = QSpacerItem(20, 20, QSizePolicy.Policy.Expanding)
            hLayout.addItem(spacer)
            return hLayout

        def createSlider(title: str, minValue: int, maxValue: int, cbValueChanged):
            vLayout = QVBoxLayout()
            hLayout = QHBoxLayout()
            lblBegin = QLabel(self.file.start_date_time_str.replace("_", " "))
            hLayout.addWidget(lblBegin)
            spacer = QSpacerItem(20, 20, QSizePolicy.Policy.Expanding)
            hLayout.addItem(spacer)
            lblTime = QLabel(self.file.start_date_time_str.replace("_", " "))
            hLayout.addWidget(lblTime)
            spacer = QSpacerItem(20, 20, QSizePolicy.Policy.Expanding)
            hLayout.addItem(spacer)
            lblEnd = QLabel(self.file.end_date_time_str.replace("_", " "))
            hLayout.addWidget(lblEnd)
            vLayout.addLayout(hLayout)
            slider = QSlider()
            slider.setOrientation(Qt.Orientation.Horizontal)
            slider.setMinimum(minValue)
            slider.setMaximum(maxValue)
            slider.valueChanged.connect(cbValueChanged)
            hLayout = QHBoxLayout()
            lbl = QLabel(title)
            lbl.setMinimumWidth(48)
            hLayout.addWidget(lbl)
            hLayout.addWidget(slider)
            vLayout.addLayout(hLayout)
            return vLayout, slider, lblBegin, lblTime, lblEnd

        def createList(title: str, lw: QListWidget):
            vLayout = QVBoxLayout()
            vLayout.addWidget(CreateTitleLabel(title))
            vLayout.addWidget(lw)
            return vLayout

        tbw = self.app.tbwDataView
        gpsPath: List[LatLngAltType] = getGPSPath([(data.date_time, data.pa1010d) for data in file.data])
        if not gpsPath:
            return
        self.yValues = {"Temperature": [data.temperature for data in file.data],
                        "Humidity": [data.humidity for data in file.data],
                        "Pressure": getPressures(file.data),
                        "CO2": [data.co2 for data in file.data],
                        "O2": [data.mix8410.u * 21.0 / 2.0 for data in file.data],
                        "NO2": [getNO2ppm(data.multigas.no2, data.temperature, data.humidity) for data in file.data],
                        "C2H5OH": [getC2H5OHppm(data.multigas.c2h5oh, data.temperature, data.humidity) for data in file.data],
                        "VOC": [getVOCppm(data.multigas.voc, data.temperature, data.humidity) for data in file.data],
                        "CO": [getCOppm(data.multigas.co, data.temperature, data.humidity) for data in file.data],
                        "PM2.5": getPm([data.pm25 for data in file.data]),
                        "PM10": getPm([data.pm10 for data in file.data]),
                        "Altitude": getGpsAltitude([data.pa1010d for data in file.data])}
        self.gpsPath = {pos.date_time: pos for pos in gpsPath}
        self.gpsTimeline = sorted(self.gpsPath)
        self.curTime = self.gpsTimeline[0]
        minValue = int(self.gpsTimeline[0].timestamp())
        maxValue = int(self.gpsTimeline[-1].timestamp())
        vLayout = QVBoxLayout()
        layout, self.sliderTime, self.lblTimeBegin, self.lblTimeTime, self.lblTimeEnd = createSlider("Time", minValue, maxValue,
                                                                                                     self.on_timeSlider_valueChanged)
        vLayout.addLayout(layout)
        layout, self.sliderStart, self.lblStartBegin, self.lblStartTime, self.lblStartEnd = createSlider("Start", minValue, maxValue - 1,
                                                                                                         self.on_startSlider_valueChanged)
        vLayout.addLayout(layout)
        layout, self.sliderEnd, self.lblEndBegin, self.lblEndTime, self.lblEndEnd = createSlider("End", minValue + 1, maxValue,
                                                                                                 self.on_endSlider_valueChanged)
        vLayout.addLayout(layout)
        hLayout = QHBoxLayout()
        hLayout.addLayout(createList("Color Reference", self.lwColorRef))
        hLayout.addLayout(createList("Sensor Signals", self.lwSensors))
        vLayout.addLayout(hLayout)
        vLayout.addLayout(createToolbar())
        widget = QWidget()
        widget.setLayout(vLayout)
        tbw.addTab(widget, "Analyze")
        self.sliderEnd.setValue(maxValue)
        self.analyzePosMarker = None

    def on_lwSensors_itemSelectionChanged(self):
        self.btnAnalyzeShowCharts.setEnabled(len(self.lwSensors.selectedItems()) > 0)

    def on_lwColorRef_itemSelectionChanged(self):
        selected = self.lwColorRef.selectedItems()
        self.colorRef = None if not selected else selected[0].text()

    def on_startSlider_valueChanged(self, value):
        if value >= self.sliderEnd.value():
            self.sliderEnd.setValue(value + 1)
        if value >= self.sliderTime.value():
            self.sliderTime.setValue(value)
        self.lblStartTime.setText(time.strftime("%Y.%m.%d %H:%M:%S", time.localtime(value)))
        self.updateAnalyzeViews()

    def on_endSlider_valueChanged(self, value):
        if value <= self.sliderStart.value():
            self.sliderStart.setValue(value - 1)
        if value <= self.sliderTime.value():
            self.sliderTime.setValue(value)
        self.lblEndTime.setText(time.strftime("%Y.%m.%d %H:%M:%S", time.localtime(value)))
        self.updateAnalyzeViews()

    def on_timeSlider_valueChanged(self, value):
        self.curTime = datetime.fromtimestamp(value)
        self.lblTimeTime.setText(time.strftime("%Y.%m.%d %H:%M:%S", time.localtime(value)))
        self.updateAnalyzeViews()

    def on_btnAnalyzeShowMap(self):
        if not self.file:
            return
        window = MapWindow(self.colorRef, self.gpsPath, self.yValues.get(self.colorRef))
        window.sigClose.connect(self.on_mapWindow_closed)
        window.updateView(*self.getSatrtEndCurTime())
        self.maps.append(window)

    def on_mapWindow_closed(self, window: QWidget):
        try:
            self.maps.remove(window)
        except:
            pass

    def on_btnAnalyzeShowCharts(self):
        if not self.file:
            return
        signals = [item.text() for item in self.lwSensors.selectedItems()]
        dateTimes = [data.date_time for data in self.file.data]
        window = ChartWindow(dateTimes, {signal: self.yValues.get(signal) for signal in signals})
        window.sigClose.connect(self.on_chartWindow_closed)
        window.updateView(*self.getSatrtEndCurTime())
        self.charts.append(window)

    def on_chartWindow_closed(self, window: QWidget):
        try:
            self.charts.remove(window)
        except:
            pass

    def getSatrtEndCurTime(self):
        curTime = self.gpsTimeline[0]
        pos = 0
        for pos, t in enumerate(self.gpsTimeline):
            if t > self.curTime:
                break
            curTime = t
        startTime = datetime.fromtimestamp(self.sliderStart.value())
        endTime = datetime.fromtimestamp(self.sliderEnd.value())
        return startTime, endTime, curTime, pos

    def updateAnalyzeViews(self):
        startTime, endTime, curTime, pos = self.getSatrtEndCurTime()
        for mapWindow in self.maps:
            mapWindow.updateView(startTime, endTime, curTime, pos)
        for chartWindow in self.charts:
            chartWindow.updateView(startTime, endTime, curTime, pos)
