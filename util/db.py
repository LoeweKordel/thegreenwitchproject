import os
import asyncio
import traceback
from asyncio import Handle
from copy import deepcopy
from datetime import timedelta
from threading import Event, Lock
from typing import Dict, Type, TypeVar, Union, Tuple, get_args, get_origin
from dataclasses import fields, is_dataclass

import ormsgpack as msgpack
from pydantic import TypeAdapter
from pydantic.json import pydantic_encoder
import zstandard as zstd
from xxhash import xxh64

from PySide6.QtCore import Signal, QObject

from multisensor import BinFile, load_bin_file

from util.eventloop import GetEventLoop

V = TypeVar("V")


class LoadBinFileFailed(Exception):

    def __init__(self, path: str, message: str):
        self.path = path
        self.message = message
        super().__init__(message)

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, deepcopy(v, memo))
        return result

    def __str__(self) -> str:
        return f"{self.path}:\n{self.message}"


def cast(T: Type[V], v: V) -> V:
    if is_dataclass(v):
        if get_origin(T) is Union:
            T = next(
                A
                for A in get_args(T)
                if A.__name__ == v.__pydantic_model__.__name__  # type: ignore
            )
        return T(  # type: ignore
            **{f.name: cast(f.type, getattr(v, f.name)) for f in fields(v)}
        )
    if isinstance(v, dict):
        for k, vv in tuple(v.items()):
            if isinstance(vv, dict) and vv.get("__type__") == "LoadBinFileFailed":
                v[k] = LoadBinFileFailed(vv["path"], vv["message"])
    return v


def serialize_msgpack(original: V) -> bytes:
    """
    Serializes a python object to msgpack. If you're having trouble, try adding a
    'to_dict' function to your class.
    For a higher level function for model storage see
    `pack_data_in_zip_file`
    """

    def default(obj):
        if hasattr(obj, "to_dict"):
            return obj.to_dict()
        if isinstance(obj, LoadBinFileFailed):
            return {"__type__": "LoadBinFileFailed",
                    "path": obj.path,
                    "message": obj.args[0]}
        return pydantic_encoder(obj)

    return msgpack.packb(original, default=default)


def deserialize_msgpack(T: Type[V], msgpack_bytes: bytes) -> V:
    """
    Deserializes objects from msgpack that were stored by
    `serialize_msgpack`.
    """
    obj = msgpack.unpackb(msgpack_bytes)
    return cast(T, TypeAdapter(T).validate_python(obj))


# noinspection PyMethodMayBeStatic
class Database(QObject):
    changed = Signal()

    def __init__(self, parent=None) -> None:
        super().__init__(parent)
        self._recordPath: str = "records"
        self._db: Dict[str, dict | LoadBinFileFailed] = {}
        self._saveTask: Handle | None = None
        self._loadTask: Handle | None = asyncio.get_event_loop().call_soon(self.load)
        self._evtLoaded: Event = Event()
        self._lock = Lock()

    @property
    def isLoaded(self) -> bool:
        return self._evtLoaded.is_set()

    def db(self) -> Dict[str, dict]:
        self._lock.acquire()
        db = deepcopy(self._db)
        self._lock.release()
        return db

    def cksum(self, pathName: str) -> int | None:
        cksum = xxh64()
        cksum.update(open(pathName, "rb").read())
        return cksum.intdigest()

    def load(self):
        with self._lock:
            if os.path.isfile("records.db"):
                try:
                    D = zstd.ZstdDecompressor()
                    with open("records.db", "rb") as F:
                        self._db = deserialize_msgpack(dict, D.decompress(F.read()))
                        for entry in self._db.values():
                            if isinstance(entry, dict):
                                entry["start"] = entry["start_date_time_str"]
                                entry["end"] = entry["end_date_time_str"]
                                entry["duration"] = timedelta(seconds=entry["duration"])
                except:
                    traceback.print_exc()

    def update(self):
        bChanged = False
        # Check for new record files
        if not os.path.exists(self._recordPath):
            os.mkdir(self._recordPath)
        with self._lock:
            for fileName in os.listdir(self._recordPath):
                pathName = f"{self._recordPath}/{fileName}"
                if pathName not in self._db:
                    try:
                        self.add(load_bin_file(pathName))
                    except:
                        self.add(LoadBinFileFailed(pathName, traceback.format_exc()))
            # Check existing record files
            for pathName in tuple(self._db):
                file = self._db[pathName]
                try:
                    cksum = self.cksum(pathName)
                    if isinstance(file, LoadBinFileFailed) or cksum != file["cksum"]:
                        # self.remove(pathName)
                        bChanged = True
                except FileNotFoundError:
                    print(f"File {pathName} not found. Removing it from database.")
                    self.remove(pathName)
                    bChanged = True
                except:
                    traceback.print_exc()
            if bChanged:
                self.changed.emit()
            self._loadTask = None
            self._evtLoaded.set()

    def save(self):
        print("Database.save")
        C = zstd.ZstdCompressor(write_content_size=True, write_checksum=True)
        data = C.compress(serialize_msgpack(self._db))
        with open("records.db", "wb") as F:
            F.write(data)

    def add(self, file: BinFile | LoadBinFileFailed):
        if isinstance(file, BinFile):
            self._db[file.path] = {
                "filename": file.filename,
                "version": file.version,
                "sw_version": file.file_header.sw_version,
                "hw_version": file.file_header.hw_version,
                "serial_nr": file.file_header.serial_nr,
                "start": file.start,
                "start_date_time_str": file.start_date_time_str,
                "end": file.end,
                "end_date_time_str": file.end_date_time_str,
                "duration": file.duration,
                "duration_str": file.duration_str,
                "cnt": file.cnt,
                "gps": file.gps_percent(False),
                "real_gps": file.gps_percent(True),
                "has_gps_str": file.has_gps_str(),
                "cksum": self.cksum(file.path)
            }
        else:
            self._db[file.path] = file
        if self._saveTask:
            self._saveTask.cancel()
        self._saveTask = GetEventLoop().call_later(1.0, self.save)

    def remove(self, obj: str | BinFile | LoadBinFileFailed):
        if isinstance(obj, (BinFile, LoadBinFileFailed)):
            obj = obj.path
        self._db.pop(obj)
        if os.path.exists(obj):
            os.remove(obj)
        if self._saveTask:
            self._saveTask.cancel()
        self._saveTask = GetEventLoop().call_later(1.0, self.save)

    @property
    def keys(self) -> Tuple[str]:
        return tuple(self._db)

    @property
    def items(self) -> Dict[str, dict]:
        return self._db.copy()

    def get(self, pathName: str) -> dict | None:
        return self._db.get(pathName)

    def get_file(self, pathName: str) -> BinFile | str:
        pathName = f"records/{os.path.basename(pathName)}"
        try:
            return load_bin_file(pathName)
        except:
            return traceback.format_exc()
