# -*- coding: utf-8 -*-
# Author: Martin Bammer
# eMail: Martin.Bammer2@at.bosch.com

import sys
import traceback
from concurrent.futures import ThreadPoolExecutor


# noinspection PyMethodMayBeStatic
class ThreadPoolExecutorStackTraced(ThreadPoolExecutor):

    def submit(self, fn, *args, **kwargs):
        """Submits the wrapped function instead of `fn`"""
        return super().submit(self._function_wrapper, fn, *args, **kwargs)

    def _function_wrapper(self, fn, *args, **kwargs):
        """Wraps `fn` in order to preserve the traceback of any kind of raised exception"""
        try:
            return fn(*args, **kwargs)
        except Exception:
            raise sys.exc_info()[0](traceback.format_exc())
