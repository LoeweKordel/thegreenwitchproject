# -*- coding: utf-8 -*-

from typing import TYPE_CHECKING

from PySide6.QtCore import Slot, Qt
from PySide6.QtWidgets import QDialog

from PySide6_loadUi import loadUi

if TYPE_CHECKING:
    from greenwitch import MainWindow


# noinspection PyCallingNonCallable
class ChangelogDialog(QDialog):

    def __init__(self, app: 'MainWindow'):
        # noinspection PyArgumentList
        super().__init__()
        self.app: 'MainWindow' = app
        loadUi(":ui/ChangelogDialog.ui", self)
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)
        try:
            self.tbChangelog.setText(open("Changelog.txt").read())
        except Exception as exc:
            self.tbChangelog.setText(str(exc))

    @Slot()
    def on_btnClose_released(self):
        self.close()
