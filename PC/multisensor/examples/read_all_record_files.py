
import os
from concurrent.futures import ThreadPoolExecutor, Future
from typing import Dict

import multisensor

recordsDir = "../../records"

futures: Dict[str, Future] = {}
with ThreadPoolExecutor() as executor:
    for fileName in sorted(os.listdir(recordsDir)):
        futures[fileName] = executor.submit(multisensor.load_bin_file, f"{recordsDir}/{fileName}")

for fileName, future in futures.items():
    try:
        file = future.result()
        print(file)
        print(file.bin_files)
        print(file.comment)
    except Exception as exc:
        print(f"{fileName} failed with:\n{exc}")
        try:
            file = multisensor.load_bin_file(f"{recordsDir}/{fileName}", force=True)
            print(file.file_header)
        except:
            pass
