
import os
import sys

import multisensor

dataDir = "../../data"

for dirName in sorted(os.listdir(dataDir)):
    keep = "--keep" in sys.argv
    paths = [f"{dataDir}/{dirName}/{fileName}" for fileName in sorted(os.listdir(f"{dataDir}/{dirName}"))]
    records, errors = multisensor.load_bin_files(sorted(paths))
    if not records and not errors:
        print(f"Remove directory {dirName}")
        if not keep:
            os.rmdir(f"{dataDir}/{dirName}")
        continue
    for fileName, file in sorted(records.items()):
        print(f"{fileName} version={file.version} start={file.start_date_time_str} duration={file.duration_str}")
    for path, error in errors.items():
        print(f"Remove {path}: {error}")
        if not keep:
            os.remove(path)
    if not keep and not records:
        os.rmdir(f"{dataDir}/{dirName}")
