use pyo3::exceptions;
use pyo3::prelude::*;
use pyo3::types::PyDateTime;

use crate::float16::f16tof32;

pub const DATA_SIZE: usize = 36; // After every 60 measurements (5 min) write them to a file.
pub const TLV493D_SIZE: usize = 100; // Size of magnet buffer (slow ~5 samples/s)
pub const ICM20600_SIZE: usize = 10; // Size of accel and gyro buffer (~2 samples/s)
pub const AK09918_SIZE: usize = 100; // Size of magnet buffer (fast ~20 samples/s)
pub const PRESSURE_SIZE: usize = 100;

#[pyclass]
#[repr(u8)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Mhz19bErrorT {
    ResultNull = 0,
    ResultOk = 1,
    ResultTimeout = 2,
    ResultMatch = 3,
    ResultCrc = 4,
    ResultFilter = 5,
}

#[pyclass]
#[repr(u8)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SdcardTypeT {
    CardNone,
    CardMmc,
    CardSd,
    CardSdhc,
    CardUnknown,
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct MsDateT {
    #[pyo3(get)]
    pub year: u16,
    #[pyo3(get)]
    pub month: u8,
    #[pyo3(get)]
    pub day: u8,
}

impl MsDateT {
    pub fn size() -> usize {
        4
    }
}

#[pymethods]
impl MsDateT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = MsDateT {
            year: 0,
            month: 0,
            day: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.year = u16::from_le_bytes(buffer[0..2].try_into()?);
        self.month = buffer[2];
        self.day = buffer[3];
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.year.to_le_bytes());
        buffer.push(self.month);
        buffer.push(self.day);
        buffer
    }

    pub fn __str__(&self) -> PyResult<String> {
        Ok(format!("{}.{}.{}", self.year, self.month, self.day))
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct MsTimeT {
    #[pyo3(get)]
    pub hour: u8,
    #[pyo3(get)]
    pub minute: u8,
    #[pyo3(get)]
    pub second: u8,
}

impl MsTimeT {
    pub fn size() -> usize {
        3
    }
}

#[pymethods]
impl MsTimeT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = MsTimeT {
            hour: 0,
            minute: 0,
            second: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.hour = buffer[0];
        self.minute = buffer[1];
        self.second = buffer[2];
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.hour);
        buffer.push(self.minute);
        buffer.push(self.second);
        buffer
    }

    pub fn __str__(&self) -> PyResult<String> {
        Ok(format!(
            "{:02}:{:02}:{:02}",
            self.hour, self.minute, self.second
        ))
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct XyzT {
    #[pyo3(get)]
    pub x: u16, // float16
    #[pyo3(get)]
    pub y: u16, // float16
    #[pyo3(get)]
    pub z: u16, // float16
}

impl XyzT {
    pub fn size() -> usize {
        6
    }
}

#[pymethods]
impl XyzT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = XyzT { x: 0, y: 0, z: 0 };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.x = u16::from_le_bytes(buffer[0..2].try_into()?);
        self.y = u16::from_le_bytes(buffer[2..4].try_into()?);
        self.z = u16::from_le_bytes(buffer[4..6].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.x.to_le_bytes());
        buffer.extend_from_slice(&self.y.to_le_bytes());
        buffer.extend_from_slice(&self.z.to_le_bytes());
        buffer
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Bmp180T {
    #[pyo3(get)]
    pub cnt: u8,
    #[pyo3(get)]
    pub temp: i16, // °C (2 decimals)
    #[pyo3(get)]
    pub pressure_raw: Vec<u16>, // hPa (1 decimal)
}

#[pymethods]
impl Bmp180T {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Bmp180T {
            cnt: 0,
            temp: 0,
            pressure_raw: Vec::new(),
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.cnt = buffer[0];
        if self.cnt as usize > PRESSURE_SIZE {
            return Err(exceptions::PyValueError::new_err(format!(
                "BMP180: cnt={} is too big!",
                self.cnt
            )));
        }
        self.temp = i16::from_le_bytes(buffer[1..3].try_into()?);
        let mut offset: usize = 3;
        for _ in 0..self.cnt {
            self.pressure_raw
                .push(u16::from_le_bytes(buffer[offset..offset + 2].try_into()?));
            offset += 2;
        }
        Ok(())
    }

    pub fn to_bytes(&self, compact: bool) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.cnt);
        buffer.extend_from_slice(&self.temp.to_le_bytes());
        for pressure in self.pressure_raw.iter() {
            buffer.extend_from_slice(&pressure.to_le_bytes());
        }
        if !compact {
            let pad = vec![0, 0];
            for _ in 0..PRESSURE_SIZE - self.pressure_raw.len() {
                buffer.extend_from_slice(&pad);
            }
        }
        buffer
    }

    pub fn pressure(&self) -> PyResult<Vec<f32>> {
        Ok(self.pressure_raw.iter().map(|x| f16tof32(*x)).collect())
    }

    pub fn size(&self, compact: bool) -> usize {
        if compact {
            3 + 2 * self.cnt as usize
        } else {
            3 + PRESSURE_SIZE * 2
        }
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Bme280T {
    #[pyo3(get)]
    pub cnt: u8,
    #[pyo3(get)]
    pub temp: i16, // °C (2 decimals)
    #[pyo3(get)]
    pub humidity: i16, // % (2 decimals)
    #[pyo3(get)]
    pub pressure_raw: Vec<u16>, // hPa (1 decimal)
}

#[pymethods]
impl Bme280T {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Bme280T {
            cnt: 0,
            temp: 0,
            humidity: 0,
            pressure_raw: Vec::new(),
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.cnt = buffer[0];
        if self.cnt as usize > PRESSURE_SIZE {
            return Err(exceptions::PyValueError::new_err(format!(
                "BME280: cnt={} is too big!",
                self.cnt
            )));
        }
        self.temp = i16::from_le_bytes(buffer[1..3].try_into()?);
        self.humidity = i16::from_le_bytes(buffer[3..5].try_into()?);
        let mut offset: usize = 5;
        if 5 + 2 * self.cnt as usize > buffer.len() {
            return Err(exceptions::PyValueError::new_err(format!(
                "BME280: buffer (size={}, cnt={}) is too small!",
                buffer.len(),
                self.cnt
            )));
        }
        for _ in 0..self.cnt {
            self.pressure_raw
                .push(u16::from_le_bytes(buffer[offset..offset + 2].try_into()?));
            offset += 2;
        }
        Ok(())
    }

    pub fn to_bytes(&self, compact: bool) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.cnt);
        buffer.extend_from_slice(&self.temp.to_le_bytes());
        buffer.extend_from_slice(&self.humidity.to_le_bytes());
        for pressure in self.pressure_raw.iter() {
            buffer.extend_from_slice(&pressure.to_le_bytes());
        }
        if !compact {
            let pad = vec![0, 0];
            for _ in 0..PRESSURE_SIZE - self.pressure_raw.len() {
                buffer.extend_from_slice(&pad);
            }
        }
        buffer
    }

    pub fn pressure(&self) -> PyResult<Vec<f32>> {
        Ok(self.pressure_raw.iter().map(|x| f16tof32(*x)).collect())
    }

    pub fn size(&self, compact: bool) -> usize {
        if compact {
            5 + 2 * self.cnt as usize
        } else {
            5 + PRESSURE_SIZE * 2
        }
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Dht20T {
    #[pyo3(get)]
    pub temp: i16, // °C (2 decimals)
    #[pyo3(get)]
    pub humidity: i16, // % (1 decimal)
}

#[pymethods]
impl Dht20T {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Dht20T {
            temp: 0,
            humidity: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.temp = i16::from_le_bytes(buffer[0..2].try_into()?);
        self.humidity = i16::from_le_bytes(buffer[2..4].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.temp.to_le_bytes());
        buffer.extend_from_slice(&self.humidity.to_le_bytes());
        buffer
    }

    pub fn size(&self) -> usize {
        4
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Tlv493dT {
    #[pyo3(get)]
    pub error: u8,
    #[pyo3(get)]
    pub cnt: u8, // Number of samples in buffer
    #[pyo3(get)]
    pub magnet_raw: Vec<XyzT>, // mT (TLV493D)
    #[pyo3(get)]
    pub temp: i16, // °C (2 decimals)
}

#[pymethods]
impl Tlv493dT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Tlv493dT {
            error: 0,
            cnt: 0,
            magnet_raw: Vec::new(),
            temp: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.error = buffer[0];
        self.cnt = buffer[1];
        if self.cnt as usize > TLV493D_SIZE {
            return Err(exceptions::PyValueError::new_err(format!(
                "TLV493D: cnt={} is too big!",
                self.cnt
            )));
        }
        let xyz_size = XyzT::size();
        let mut offset: usize = 2;
        for _ in 0..self.cnt {
            if buffer[offset..].len() < xyz_size {
                return Err(exceptions::PyValueError::new_err(
                    "TLV493D: buffer for magnet data too small!".to_owned(),
                ));
            }
            self.magnet_raw.push(XyzT::new(Some(&buffer[offset..]))?);
            offset += xyz_size;
        }
        self.temp = i16::from_le_bytes(buffer[offset..offset + 2].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self, compact: bool) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.error);
        buffer.push(self.cnt);
        for xyz in self.magnet_raw.iter() {
            buffer.extend_from_slice(&xyz.to_bytes());
        }
        if !compact {
            let pad = vec![0, 0, 0, 0, 0, 0];
            for _ in 0..TLV493D_SIZE - self.magnet_raw.len() {
                buffer.extend_from_slice(&pad);
            }
        }
        buffer.extend_from_slice(&self.temp.to_le_bytes());
        buffer
    }

    #[getter]
    pub fn magnet(&self) -> PyResult<Vec<(f32, f32, f32)>> {
        Ok(self
            .magnet_raw
            .iter()
            .map(|xyz| (f16tof32(xyz.x), f16tof32(xyz.y), f16tof32(xyz.z)))
            .collect())
    }

    pub fn size(&self, compact: bool) -> usize {
        if compact {
            4 + self.cnt as usize * XyzT::size()
        } else {
            4 + TLV493D_SIZE * XyzT::size()
        }
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Imu9dofT {
    #[pyo3(get)]
    pub icm20600_cnt: u8,
    #[pyo3(get)]
    pub ak09918_cnt: u8,
    #[pyo3(get)]
    pub ak09918_error: u8,
    #[pyo3(get)]
    pub accel_raw: Vec<XyzT>, // m/s^2
    #[pyo3(get)]
    pub gyro_raw: Vec<XyzT>, // rad/s
    #[pyo3(get)]
    pub magnet_raw: Vec<XyzT>, // uT
    #[pyo3(get)]
    pub temp: i16, // °C (2 decimals)
}

#[pymethods]
impl Imu9dofT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Imu9dofT {
            icm20600_cnt: 0,
            ak09918_cnt: 0,
            ak09918_error: 0,
            accel_raw: Vec::new(),
            gyro_raw: Vec::new(),
            magnet_raw: Vec::new(),
            temp: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.icm20600_cnt = buffer[0];
        if self.icm20600_cnt as usize > ICM20600_SIZE {
            return Err(exceptions::PyValueError::new_err(format!(
                "IMU9DOF: icm20600_cnt={} is too big!",
                self.icm20600_cnt
            )));
        }
        self.ak09918_cnt = buffer[1];
        if self.ak09918_cnt as usize > AK09918_SIZE {
            return Err(exceptions::PyValueError::new_err(format!(
                "IMU9DOF: ak09918_cnt={} is too big!",
                self.ak09918_cnt
            )));
        }
        self.ak09918_error = buffer[2];
        let xyz_size = XyzT::size();
        let mut offset: usize = 3;
        for _ in 0..self.icm20600_cnt {
            if buffer[offset..].len() < xyz_size {
                return Err(exceptions::PyValueError::new_err(
                    "IMU9DOF: buffer for icm20600 accel data too small!".to_owned(),
                ));
            }
            self.accel_raw.push(XyzT::new(Some(&buffer[offset..]))?);
            offset += xyz_size;
        }
        for _ in 0..self.icm20600_cnt {
            if buffer[offset..].len() < xyz_size {
                return Err(exceptions::PyValueError::new_err(
                    "IMU9DOF: buffer for icm20600 gyro data too small!".to_owned(),
                ));
            }
            self.gyro_raw.push(XyzT::new(Some(&buffer[offset..]))?);
            offset += xyz_size;
        }
        for _ in 0..self.ak09918_cnt {
            if buffer[offset..].len() < xyz_size {
                return Err(exceptions::PyValueError::new_err(
                    "IMU9DOF: buffer for ak09918_cnt magnet data too small!".to_owned(),
                ));
            }
            self.magnet_raw.push(XyzT::new(Some(&buffer[offset..]))?);
            offset += xyz_size;
        }
        self.temp = i16::from_le_bytes(buffer[offset..offset + 2].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self, compact: bool) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.icm20600_cnt);
        buffer.push(self.ak09918_cnt);
        buffer.push(self.ak09918_error);
        for xyz in self.accel_raw.iter() {
            buffer.extend_from_slice(&xyz.to_bytes());
        }
        if !compact {
            let pad = vec![0, 0, 0, 0, 0, 0];
            for _ in 0..ICM20600_SIZE - self.accel_raw.len() {
                buffer.extend_from_slice(&pad);
            }
        }
        for xyz in self.gyro_raw.iter() {
            buffer.extend_from_slice(&xyz.to_bytes());
        }
        if !compact {
            let pad = vec![0, 0, 0, 0, 0, 0];
            for _ in 0..ICM20600_SIZE - self.gyro_raw.len() {
                buffer.extend_from_slice(&pad);
            }
        }
        for xyz in self.magnet_raw.iter() {
            buffer.extend_from_slice(&xyz.to_bytes());
        }
        if !compact {
            let pad = vec![0, 0, 0, 0, 0, 0];
            for _ in 0..AK09918_SIZE - self.magnet_raw.len() {
                buffer.extend_from_slice(&pad);
            }
        }
        buffer.extend_from_slice(&self.temp.to_le_bytes());
        buffer
    }

    #[getter]
    pub fn accel(&self) -> PyResult<Vec<(f32, f32, f32)>> {
        Ok(self
            .accel_raw
            .iter()
            .map(|xyz| (f16tof32(xyz.x), f16tof32(xyz.y), f16tof32(xyz.z)))
            .collect())
    }

    #[getter]
    pub fn gyro(&self) -> PyResult<Vec<(f32, f32, f32)>> {
        Ok(self
            .gyro_raw
            .iter()
            .map(|xyz| (f16tof32(xyz.x), f16tof32(xyz.y), f16tof32(xyz.z)))
            .collect())
    }

    #[getter]
    pub fn magnet(&self) -> PyResult<Vec<(f32, f32, f32)>> {
        Ok(self
            .magnet_raw
            .iter()
            .map(|xyz| (f16tof32(xyz.x), f16tof32(xyz.y), f16tof32(xyz.z)))
            .collect())
    }

    pub fn size(&self, compact: bool) -> usize {
        if compact {
            5 + (2 * self.icm20600_cnt as usize + self.ak09918_cnt as usize) * XyzT::size()
        } else {
            5 + (2 * ICM20600_SIZE + AK09918_SIZE) * XyzT::size()
        }
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Sds011T {
    #[pyo3(get)]
    pub valid: bool,
    #[pyo3(get)]
    pub pm25: i16, // [ug/m3] PM2.5 (1 decimal)
    #[pyo3(get)]
    pub pm10: i16, // [ug/m3] PM10 (1 decimal)
}

#[pymethods]
impl Sds011T {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Sds011T {
            valid: false,
            pm25: 0,
            pm10: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.valid = buffer[0] != 0;
        self.pm25 = i16::from_le_bytes(buffer[1..3].try_into()?);
        self.pm10 = i16::from_le_bytes(buffer[3..5].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.push(self.valid as u8);
        buffer.extend_from_slice(&self.pm25.to_le_bytes());
        buffer.extend_from_slice(&self.pm10.to_le_bytes());
        buffer
    }

    pub fn size(&self) -> usize {
        5
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Mhz19bT {
    #[pyo3(get)]
    pub co2: i16, // CO2 in ppm (0 decimals)
    #[pyo3(get)]
    pub temp: i16, // °C (2 decimals)
    #[pyo3(get)]
    pub error: u8,
}

#[pymethods]
impl Mhz19bT {
    #[new]
    pub fn new(buffer: Option<&[u8]>, data_version: Option<u8>) -> PyResult<Self> {
        let mut result = Mhz19bT {
            co2: 0,
            temp: 0,
            error: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer, data_version)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8], data_version: Option<u8>) -> PyResult<()> {
        self.co2 = i16::from_le_bytes(buffer[0..2].try_into()?);
        self.temp = i16::from_le_bytes(buffer[2..4].try_into()?);
        if data_version.unwrap_or(6) > 6 {
            self.error = buffer[4];
        }
        Ok(())
    }

    pub fn to_bytes(&self, data_version: Option<u8>) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.co2.to_le_bytes());
        buffer.extend_from_slice(&self.temp.to_le_bytes());
        if data_version.unwrap_or(6) > 6 {
            buffer.push(self.error);
        }
        buffer
    }

    pub fn size(&self, data_version: Option<u8>) -> usize {
        if data_version.unwrap_or(6) > 6 {
            5
        } else {
            4
        }
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct Mix8410T {
    #[pyo3(get)]
    pub u: f32, // V
}

#[pymethods]
impl Mix8410T {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = Mix8410T { u: 0.0 };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.u = f32::from_le_bytes(buffer[0..4].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.u.to_le_bytes());
        buffer
    }

    pub fn size(&self) -> usize {
        4
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct MultiGasT {
    #[pyo3(get)]
    pub no2: u32, // Raw value
    #[pyo3(get)]
    pub c2h5oh: u32, // Raw value
    #[pyo3(get)]
    pub voc: u32, // Raw value
    #[pyo3(get)]
    pub co: u32, // Raw value
}

#[pymethods]
impl MultiGasT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = MultiGasT {
            no2: 0,
            c2h5oh: 0,
            voc: 0,
            co: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.no2 = u32::from_le_bytes(buffer[0..4].try_into()?);
        self.c2h5oh = u32::from_le_bytes(buffer[4..8].try_into()?);
        self.voc = u32::from_le_bytes(buffer[8..12].try_into()?);
        self.co = u32::from_le_bytes(buffer[12..16].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.no2.to_le_bytes());
        buffer.extend_from_slice(&self.c2h5oh.to_le_bytes());
        buffer.extend_from_slice(&self.voc.to_le_bytes());
        buffer.extend_from_slice(&self.co.to_le_bytes());
        buffer
    }

    pub fn size(&self) -> usize {
        16
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct GpsT {
    #[pyo3(get)]
    pub date: MsDateT,
    #[pyo3(get)]
    pub time: MsTimeT,
    #[pyo3(get)]
    pub satellites: u8,
    #[pyo3(get)]
    pub latitude: f32,
    #[pyo3(get)]
    pub longitude: f32,
    #[pyo3(get)]
    pub altitude: u16, // m (1 decimals)
    #[pyo3(get)]
    pub speed: u16, // km/h (1 decimals)
    #[pyo3(get)]
    pub angle: f32,
    #[pyo3(get)]
    pub hdop: f32,
    #[pyo3(get)]
    pub vdop: f32,
    #[pyo3(get)]
    pub pdop: f32,
    #[pyo3(get)]
    pub valid: bool, // Have a fix?
    #[pyo3(get)]
    pub quality: u8, // Fix quality (0, 1, 2 = Invalid, GPS, DGPS)
    #[pyo3(get)]
    pub quality_3d: u8, // 3D fix quality (1, 3, 3 = Nofix, 2D fix, 3D fix)
}

#[pymethods]
impl GpsT {
    #[new]
    pub fn new(buffer: Option<&[u8]>) -> PyResult<Self> {
        let mut result = GpsT {
            date: MsDateT::new(None)?,
            time: MsTimeT::new(None)?,
            satellites: 0,
            latitude: 0.0,
            longitude: 0.0,
            altitude: 0,
            speed: 0,
            angle: 0.0,
            hdop: 0.0,
            vdop: 0.0,
            pdop: 0.0,
            valid: false,
            quality: 0,
            quality_3d: 0,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer)?;
        }
        Ok(result)
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.date.from_bytes(&buffer[0..])?;
        let mut offset = MsDateT::size();
        self.time.from_bytes(&buffer[offset..])?;
        offset += MsTimeT::size();
        self.satellites = buffer[offset];
        self.latitude = f32::from_le_bytes(buffer[offset + 1..offset + 5].try_into()?);
        self.longitude = f32::from_le_bytes(buffer[offset + 5..offset + 9].try_into()?);
        self.altitude = u16::from_le_bytes(buffer[offset + 9..offset + 11].try_into()?);
        self.speed = u16::from_le_bytes(buffer[offset + 11..offset + 13].try_into()?);
        self.angle = f32::from_le_bytes(buffer[offset + 13..offset + 17].try_into()?);
        self.hdop = f32::from_le_bytes(buffer[offset + 17..offset + 21].try_into()?);
        self.vdop = f32::from_le_bytes(buffer[offset + 21..offset + 25].try_into()?);
        self.pdop = f32::from_le_bytes(buffer[offset + 25..offset + 29].try_into()?);
        self.valid = buffer[offset + 29] != 0;
        self.quality = buffer[offset + 30];
        self.quality_3d = buffer[offset + 31];
        Ok(())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.date.to_bytes());
        buffer.extend_from_slice(&self.time.to_bytes());
        buffer.push(self.satellites);
        buffer.extend_from_slice(&self.latitude.to_le_bytes());
        buffer.extend_from_slice(&self.longitude.to_le_bytes());
        buffer.extend_from_slice(&self.altitude.to_le_bytes());
        buffer.extend_from_slice(&self.speed.to_le_bytes());
        buffer.extend_from_slice(&self.angle.to_le_bytes());
        buffer.extend_from_slice(&self.hdop.to_le_bytes());
        buffer.extend_from_slice(&self.vdop.to_le_bytes());
        buffer.extend_from_slice(&self.pdop.to_le_bytes());
        buffer.push(self.valid as u8);
        buffer.push(self.quality);
        buffer.push(self.quality_3d);
        buffer
    }

    pub fn size(&self) -> usize {
        39
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }

    fn __repr__(&self) -> PyResult<String> {
        self.__str__()
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct DataT {
    #[pyo3(get)]
    pub counter: u32, // 4 byte
    #[pyo3(get)]
    pub date: MsDateT, // 4 byte
    #[pyo3(get)]
    pub time: MsTimeT, // 3 byte
    #[pyo3(get)]
    pub bmp180: Bmp180T, // 202 byte
    #[pyo3(get)]
    pub bme280: Bme280T, // 204 byte
    #[pyo3(get)]
    pub dht20: Dht20T, // 4 byte
    #[pyo3(get)]
    pub tlv493d: Tlv493dT, // 604 byte
    #[pyo3(get)]
    pub imu9dof: Imu9dofT, // 725 byte
    #[pyo3(get)]
    pub sds011: Sds011T, // 5 byte
    #[pyo3(get)]
    pub mhz19b: Mhz19bT, // 4/5 byte
    #[pyo3(get)]
    pub mix8410: Mix8410T, // 4 byte
    #[pyo3(get)]
    pub multigas: MultiGasT, // 16 byte
    #[pyo3(get)]
    pub pa1010d: GpsT, // 39 byte
} // 1820/1821 byte

#[pymethods]
impl DataT {
    #[new]
    pub fn new(
        buffer: Option<&[u8]>,
        compact: Option<bool>,
        data_version: Option<u8>,
    ) -> PyResult<Self> {
        let mut result = DataT {
            counter: 0,
            date: MsDateT::new(None)?,
            time: MsTimeT::new(None)?,
            bmp180: Bmp180T::new(None)?,
            bme280: Bme280T::new(None)?,
            dht20: Dht20T::new(None)?,
            tlv493d: Tlv493dT::new(None)?,
            imu9dof: Imu9dofT::new(None)?,
            sds011: Sds011T::new(None)?,
            mhz19b: Mhz19bT::new(None, data_version)?,
            mix8410: Mix8410T::new(None)?,
            multigas: MultiGasT::new(None)?,
            pa1010d: GpsT::new(None)?,
        };
        if let Some(buffer) = buffer {
            result.from_bytes(buffer, compact.unwrap_or(false), data_version)?;
        }
        Ok(result)
    }

    pub fn from_bytes(
        &mut self,
        buffer: &[u8],
        compact: bool,
        data_version: Option<u8>,
    ) -> PyResult<()> {
        if buffer.len() < self.min_size(data_version) {
            return Err(exceptions::PyValueError::new_err(format!(
                "DataT.from_bytes: buffer (size={}) too small!",
                buffer.len()
            )));
        }
        self.counter = u32::from_le_bytes(buffer[0..4].try_into()?);
        self.date = MsDateT::new(Some(&buffer[4..]))?;
        self.time = MsTimeT::new(Some(&buffer[8..]))?;
        self.bmp180 = Bmp180T::new(Some(&buffer[11..]))?;
        let mut offset = 11 + self.bmp180.size(compact);
        if buffer.len() <= offset {
            return Err(exceptions::PyValueError::new_err(format!(
                "DataT.from_bytes: buffer (size={}) too small for BME280!",
                buffer.len()
            )));
        }
        self.bme280 = Bme280T::new(Some(&buffer[offset..]))?;
        offset += self.bme280.size(compact);
        if buffer.len() <= offset {
            return Err(exceptions::PyValueError::new_err(format!(
                "DataT.from_bytes: buffer (size={}) too small for DHT20!",
                buffer.len()
            )));
        }
        self.dht20 = Dht20T::new(Some(&buffer[offset..]))?;
        offset += self.dht20.size();
        if buffer.len() <= offset {
            return Err(exceptions::PyValueError::new_err(format!(
                "DataT.from_bytes: buffer (size={}) too small for TLV493D!",
                buffer.len()
            )));
        }
        self.tlv493d = Tlv493dT::new(Some(&buffer[offset..]))?;
        offset += self.tlv493d.size(compact);
        if buffer.len() <= offset {
            return Err(exceptions::PyValueError::new_err(format!(
                "DataT.from_bytes: buffer (size={}) too small for IMU9DOF!",
                buffer.len()
            )));
        }
        self.imu9dof = Imu9dofT::new(Some(&buffer[offset..]))?;
        offset += self.imu9dof.size(compact);
        if buffer.len() < offset + 68 {
            return Err(exceptions::PyValueError::new_err(format!(
                "DataT.from_bytes: buffer (size={}) too small for SDS011..!",
                buffer.len() as i32 - offset as i32
            )));
        }
        self.sds011 = Sds011T::new(Some(&buffer[offset..]))?;
        offset += self.sds011.size();
        self.mhz19b = Mhz19bT::new(Some(&buffer[offset..]), data_version)?;
        offset += self.mhz19b.size(data_version);
        self.mix8410 = Mix8410T::new(Some(&buffer[offset..]))?;
        offset += self.mix8410.size();
        self.multigas = MultiGasT::new(Some(&buffer[offset..]))?;
        offset += self.multigas.size();
        self.pa1010d = GpsT::new(Some(&buffer[offset..]))?;
        Ok(())
    }

    pub fn to_bytes(&self, compact: bool, data_version: Option<u8>) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.counter.to_le_bytes());
        buffer.extend_from_slice(&self.date.to_bytes());
        buffer.extend_from_slice(&self.time.to_bytes());
        buffer.extend_from_slice(&self.bmp180.to_bytes(compact));
        buffer.extend_from_slice(&self.bme280.to_bytes(compact));
        buffer.extend_from_slice(&self.dht20.to_bytes());
        buffer.extend_from_slice(&self.tlv493d.to_bytes(compact));
        buffer.extend_from_slice(&self.imu9dof.to_bytes(compact));
        buffer.extend_from_slice(&self.sds011.to_bytes());
        buffer.extend_from_slice(&self.mhz19b.to_bytes(data_version));
        buffer.extend_from_slice(&self.mix8410.to_bytes());
        buffer.extend_from_slice(&self.multigas.to_bytes());
        buffer.extend_from_slice(&self.pa1010d.to_bytes());
        buffer
    }

    #[getter]
    pub fn date_time(&self, py: Python) -> PyResult<Py<PyAny>> {
        let result = PyDateTime::new(
            py,
            self.date.year as i32,
            self.date.month,
            self.date.day,
            self.time.hour,
            self.time.minute,
            self.time.second,
            0,
            None,
        )
        .unwrap()
        .to_object(py);
        Ok(result)
    }

    #[getter]
    pub fn temperature(&self) -> PyResult<f32> {
        Ok((self.bmp180.temp + self.dht20.temp + self.tlv493d.temp) as f32 / 300.0)
    }

    #[getter]
    pub fn humidity(&self) -> PyResult<f32> {
        Ok((self.bme280.humidity + self.dht20.humidity) as f32 / 20.0)
    }

    #[getter]
    pub fn pressure(&self) -> PyResult<Vec<f32>> {
        Ok((self
            .bmp180
            .pressure_raw
            .iter()
            .zip(self.bme280.pressure_raw.iter())
            .map(|(x, y)| 0.5 * (f16tof32(*x) + f16tof32(*y))))
        .collect())
    }

    #[getter]
    pub fn co2(&self) -> PyResult<i16> {
        Ok(self.mhz19b.co2)
    }

    #[getter]
    pub fn pm25(&self) -> PyResult<f32> {
        Ok(self.sds011.pm25 as f32 / 10.0)
    }

    #[getter]
    pub fn pm10(&self) -> PyResult<f32> {
        Ok(self.sds011.pm10 as f32 / 10.0)
    }

    pub fn size(&self, compact: bool, data_version: Option<u8>) -> usize {
        if compact {
            let base_size = if data_version.unwrap_or(6) > 6 {
                84
            } else {
                83
            };
            base_size
                + self.bmp180.size(compact)
                + self.bme280.size(compact)
                + self.tlv493d.size(compact)
                + self.imu9dof.size(compact)
        } else {
            if data_version.unwrap_or(6) > 6 {
                1821
            } else {
                1820
            }
        }
    }

    pub fn min_size(&self, data_version: Option<u8>) -> usize {
        if data_version.unwrap_or(6) > 6 {
            129
        } else {
            128
        }
    }
}

#[pyclass]
#[derive(Debug, Clone)]
pub struct OffsetsT {
    #[pyo3(get)]
    pub bmp180_temp: f32,
    #[pyo3(get)]
    pub bmp180_pressure: f32,
    #[pyo3(get)]
    pub bme280_temp: f32,
    #[pyo3(get)]
    pub bme280_pressure: f32,
    #[pyo3(get)]
    pub bme280_humidity: f32,
    #[pyo3(get)]
    pub dht20_temp: f32,
    #[pyo3(get)]
    pub dht20_humidity: f32,
    #[pyo3(get)]
    pub mhz19_temp: f32,
    #[pyo3(get)]
    pub mhz19_co2: f32,
    #[pyo3(get)]
    pub mix8410_u: f32,
    #[pyo3(get)]
    pub multigas_no2: f32,
    #[pyo3(get)]
    pub multigas_c2h5oh: f32,
    #[pyo3(get)]
    pub multigas_voc: f32,
    #[pyo3(get)]
    pub multigas_co: f32,
    #[pyo3(get)]
    pub tlv493d_temp: f32,
    #[pyo3(get)]
    pub imu9dof_axe: f32,
    #[pyo3(get)]
    pub imu9dof_aye: f32,
    #[pyo3(get)]
    pub imu9dof_aze: f32,
    #[pyo3(get)]
    pub imu9dof_gxe: f32,
    #[pyo3(get)]
    pub imu9dof_gye: f32,
    #[pyo3(get)]
    pub imu9dof_gze: f32,
    #[pyo3(get)]
    pub imu9dof_temp: f32,
}

impl OffsetsT {
    pub fn size() -> usize {
        88
    }
}

#[pymethods]
impl OffsetsT {
    #[new]
    pub fn new() -> Self {
        OffsetsT {
            bmp180_temp: 0.0,
            bmp180_pressure: 0.0,
            bme280_temp: 0.0,
            bme280_pressure: 0.0,
            bme280_humidity: 0.0,
            dht20_temp: 0.0,
            dht20_humidity: 0.0,
            mhz19_temp: 0.0,
            mhz19_co2: 0.0,
            mix8410_u: 0.0,
            multigas_no2: 0.0,
            multigas_c2h5oh: 0.0,
            multigas_voc: 0.0,
            multigas_co: 0.0,
            tlv493d_temp: 0.0,
            imu9dof_axe: 0.0,
            imu9dof_aye: 0.0,
            imu9dof_aze: 0.0,
            imu9dof_gxe: 0.0,
            imu9dof_gye: 0.0,
            imu9dof_gze: 0.0,
            imu9dof_temp: 0.0,
        }
    }

    pub fn from_bytes(&mut self, buffer: &[u8]) -> PyResult<()> {
        self.bmp180_temp = f32::from_le_bytes(buffer[0..4].try_into()?);
        self.bmp180_pressure = f32::from_le_bytes(buffer[4..8].try_into()?);
        self.bme280_temp = f32::from_le_bytes(buffer[8..12].try_into()?);
        self.bme280_pressure = f32::from_le_bytes(buffer[12..16].try_into()?);
        self.bme280_humidity = f32::from_le_bytes(buffer[16..20].try_into()?);
        self.dht20_temp = f32::from_le_bytes(buffer[20..24].try_into()?);
        self.dht20_humidity = f32::from_le_bytes(buffer[24..28].try_into()?);
        self.mhz19_temp = f32::from_le_bytes(buffer[28..32].try_into()?);
        self.mhz19_co2 = f32::from_le_bytes(buffer[32..36].try_into()?);
        self.mix8410_u = f32::from_le_bytes(buffer[36..40].try_into()?);
        self.multigas_no2 = f32::from_le_bytes(buffer[40..44].try_into()?);
        self.multigas_c2h5oh = f32::from_le_bytes(buffer[44..48].try_into()?);
        self.multigas_voc = f32::from_le_bytes(buffer[48..52].try_into()?);
        self.multigas_co = f32::from_le_bytes(buffer[52..56].try_into()?);
        self.tlv493d_temp = f32::from_le_bytes(buffer[56..60].try_into()?);
        self.imu9dof_axe = f32::from_le_bytes(buffer[60..64].try_into()?);
        self.imu9dof_aye = f32::from_le_bytes(buffer[64..68].try_into()?);
        self.imu9dof_aze = f32::from_le_bytes(buffer[68..72].try_into()?);
        self.imu9dof_gxe = f32::from_le_bytes(buffer[72..76].try_into()?);
        self.imu9dof_gye = f32::from_le_bytes(buffer[76..80].try_into()?);
        self.imu9dof_gze = f32::from_le_bytes(buffer[80..84].try_into()?);
        self.imu9dof_temp = f32::from_le_bytes(buffer[84..88].try_into()?);
        Ok(())
    }

    pub fn to_bytes(&mut self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        buffer.extend_from_slice(&self.bmp180_temp.to_le_bytes());
        buffer.extend_from_slice(&self.bmp180_pressure.to_le_bytes());
        buffer.extend_from_slice(&self.bme280_temp.to_le_bytes());
        buffer.extend_from_slice(&self.bme280_pressure.to_le_bytes());
        buffer.extend_from_slice(&self.bme280_humidity.to_le_bytes());
        buffer.extend_from_slice(&self.dht20_temp.to_le_bytes());
        buffer.extend_from_slice(&self.dht20_humidity.to_le_bytes());
        buffer.extend_from_slice(&self.mhz19_temp.to_le_bytes());
        buffer.extend_from_slice(&self.mhz19_co2.to_le_bytes());
        buffer.extend_from_slice(&self.mix8410_u.to_le_bytes());
        buffer.extend_from_slice(&self.multigas_no2.to_le_bytes());
        buffer.extend_from_slice(&self.multigas_c2h5oh.to_le_bytes());
        buffer.extend_from_slice(&self.multigas_voc.to_le_bytes());
        buffer.extend_from_slice(&self.multigas_co.to_le_bytes());
        buffer.extend_from_slice(&self.tlv493d_temp.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_axe.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_aye.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_aze.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_gxe.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_gye.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_gze.to_le_bytes());
        buffer.extend_from_slice(&self.imu9dof_temp.to_le_bytes());
        buffer
    }
}
