pub const INFINITY: u16 = 0x7C00;
pub const NEG_INFINITY: u16 = 0xFC00;
pub const NAN: u16 = 0xFE00;
pub const NEG_ZERO: u16 = 0x8000;

pub fn f16tof32(value: u16) -> f32 {
    let sgn: bool = (value & 0x8000) > 0;
    let man: u16 = value & 0x03FF;
    let exp: i8 = ((value & 0x7C00) >> 10) as i8;

    // ZERO
    if (value & 0x7FFF) == 0 {
        return match sgn {
            true => -0.0,
            false => 0.0,
        };
    }
    // NAN & INF
    if exp == 0x1F {
        if man == 0 {
            return match sgn {
                true => -std::f32::INFINITY,
                false => std::f32::INFINITY,
            };
        } else {
            return std::f32::NAN;
        }
    }
    // SUBNORMAL/NORMAL
    let mut f: f32 = match exp {
        0 => 0.0,
        _ => 1.0,
    };

    // PROCESS MANTISSE
    for i in 0..=9 {
        f *= 2.0;
        if man & (1 << (9 - i)) != 0 {
            f += 1.0;
        }
    }
    f *= 2.0_f32.powf((exp - 25) as f32);
    if exp == 0 {
        f *= 2.0_f32.powf(-13.0); // 5.96046447754e-8;
    }
    match sgn {
        true => -f,
        false => f,
    }
}

pub fn f32tof16(f: f32) -> u16 {
    let t: u32 = f.to_bits();
    // man bits = 10; but we keep 11 for rounding
    let mut man: u16 = ((t & 0x007FFFFF) >> 12) as u16;
    let mut exp: i16 = ((t & 0x7F800000) >> 23) as i16;
    let sgn: bool = (t & 0x80000000) != 0;

    // handle 0
    if (t & 0x7FFFFFFF) == 0 {
        return match sgn {
            true => NEG_ZERO,
            false => 0,
        };
    }
    // denormalized float32 does not fit in float16
    if exp == 0 {
        return match sgn {
            true => NEG_ZERO,
            false => 0,
        };
    }
    // handle infinity & NAN
    if exp == 0xFF {
        if man != 0 {
            return NAN; //  NAN
        }
        return match sgn {
            true => NEG_INFINITY,
            false => INFINITY,
        };
    }
    // normal numbers
    exp = exp - 127 + 15;
    // overflow does not fit => INF
    if exp > 30 {
        return match sgn {
            true => NEG_INFINITY,
            false => INFINITY,
        };
    }
    //  subnormal numbers
    if exp < -38 {
        return match sgn {
            true => NEG_ZERO,
            false => 0,
        };
    }
    if exp <= 0 {
        // subnormal
        man >>= exp + 14;
        // rounding
        man += 1;
        man >>= 1;
        if sgn {
            return 0x8000 | man;
        }
        return man;
    }

    // normal
    // TODO rounding
    exp <<= 10;
    man += 1;
    man >>= 1;
    if sgn {
        return 0x8000 | exp as u16 | man;
    }
    return exp as u16 | man;
}
