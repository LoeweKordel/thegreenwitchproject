use multisensor::float16::{f16tof32, f32tof16};

#[test]
fn test_count() {
    let f = f32tof16(0.0);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(0.001);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-0.001);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(0.01);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-0.01);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(0.1);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-0.1);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(1.0);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-1.0);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(1234.56789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-1234.56789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(12345.6789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-12345.6789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(65504.789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-65504.789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(65505.789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-65505.789);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(65519.0);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-65519.0);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(65520.0);
    println!("{f:x} {}", f16tof32(f));
    let f = f32tof16(-65520.0);
    println!("{f:x} {}", f16tof32(f));
}
